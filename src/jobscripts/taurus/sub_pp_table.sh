#!/bin/bash
#PBS -N pair_pot_table
#PBS -l nodes=1:ppn=1
#PBS -l walltime=72:00:00
#PBS -q romeo
#PBS -j oe
#PBS -e PIMC_$PBS_JOBID.err
#PBS -o PIMC_$PBS_JOBID.out


. /home/s7549247/spack/share/spack/setup-env.sh



module load GCCcore/7.3.0
module load boost-1.74.0-gcc-7.3.0-lbhjqkn
module load gsl-2.5-gcc-7.3.0-wpw35rb
module load cmake-3.18.2-gcc-7.3.0-3yj42o7
module load hdf5-1.10.6-gcc-7.3.0-kixx7k6


./pp_table_creator > logfile_pp_table
