#include "density_matrix_partial_wave.h"
#include <iostream>
#include "gsl/gsl_errno.h"
#include "density_mat.hpp"
#include "density_mat_qs.hpp"


int main()
{
    double z1 = 1.0;
    double z2 = 1.0;
    double theta = 0.0;
    double beta = 1.0;
    double P = 100;
    double epsilon = beta / P;
    double m1 = 1.0;
    double m2 = 1.0;//1836.15;
    double mu = m1*m2 / (m1 + m2);
    double kappa = 1/(2 * mu);
    double z = z1 * z2 / kappa;

    double res;
    gsl_set_error_handler_off();

    double r1 = 0.5;
    double r2 = 0.5;
    res  = rho_coulomb_partial_expansion(r1, r2, theta, beta, kappa, z, 26, 5000, 100);
    double q =  0.5 * (r1 + r2);
    double s = sqrt(r1*r1 + r2 * r2 - 2 * r1 * r2 * cos(theta));

    double result_qs = rho_coulomb_qs(q, s, beta, kappa, z, 6000, 1500, 1e-12);
    double result_reg = rho_coulomb(r1, r2, theta, beta, kappa, z, 5000, 1200, 1e-12);

    std::cout << "Result partial wave expansion: " << res << std::endl;
    std::cout << "Result qs calculation: " << result_qs << std::endl;
    std::cout << "Result reg. calculation: " << result_reg << std::endl;
    std::cout << "Result ratio: " << result_qs / res << std::endl;

    double action_partial = u_qs(res, s, kappa, beta);
    double action_qs = u_qs(result_qs, s, kappa, beta);
    double action_reg = u_qs(result_reg, s, kappa, beta);

    std::cout << "Result partial wave action: " << action_partial << std::endl;
    std::cout << "Result qs calculation:  " << action_qs << std::endl;
    std::cout << "Result reg. calculation: " << action_reg << std::endl;

}