#pragma once

#include <string>
#include <array>
#include <iostream>
#include <vector>
#include <fstream>
#include <cassert>
#include <iomanip>

#include <gsl/gsl_multifit.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_spline.h>

#include "density_mat_qs.hpp"
#include "density_mat_qs_beta_deriv.hpp"



class pair_action_lookup_table
{
public:

    // Constructor that does the pair-action interpolation and generates a readable file of the interpolated values
    pair_action_lookup_table(double z1_, double z2_, double m1_, double m2_, double l_, double beta_, int n_s_, int n_q_,
                             int polydeg_, int P_, int n_max_int_ = 5000, int n_max_sum_ = 1500, double tol_sum_ = 1e-12, double lambda_max_ = 3.0);

    // Constructor that reads a pair-action file and generates the associated values from the file
    pair_action_lookup_table(const std::string& filename, const std::string& filename_deriv, double z1_, double z2_, double m1_, double m2_, double l_, double beta_, int n_s_, int n_q_,
                             int polydeg_, int P_, int n_max_int_ = 5000, int n_max_sum_ = 1500, double tol_sum_ = 1e-12, double lambda_max_ = 3.0);

    // This constructor has the option to only calculate the associated beta-derivatives
    pair_action_lookup_table(double z1_, double z2_, double m1_, double m2_, double l_, double beta_, int n_s_, int n_q_,
                             int polydeg_, int P_, bool calc_deriv = true, int n_max_int_ = 5000, int n_max_sum_ = 1500, double tol_sum_ = 1e-12, double lambda_max_ = 3.0);

    pair_action_lookup_table() = default;
    double interpolate(double q, double s);
    double interpolate_deriv(double q, double s);
    ~pair_action_lookup_table();

    void print_coeff_mat();

    void print_diagonal_interpolation(int num_points, std::string& fname);
    void print_off_diag_expansion(double q, int num_s, std::string& fname);
    void print_off_diag_expansion_deriv(double q, int num_s, std::string& fname);

    void print_coefficient_interpolation(int c_id, std::string& fname);
    void print_interpolation_table(std::string& fname);
    void print_deriv_interpolation_table(std::string& fname);
    std::vector<double> get_coeff_mat_column(int idx);
    std::vector<double> get_coeff_deriv_mat_columns(int idx);

    double calc_pair_action(std::vector<double>& a, std::vector<double>& b);
    double calc_pair_action_deriv(std::vector<double>& a, std::vector<double>& b);
    std::vector<double> get_pair_action(std::vector<double>& a, std::vector<double>& b);
    double lambda;
    double lambda_max;


private:
    double z1, z2, m1, m2, l, beta;
    int n_s, n_q, polydeg, n_max_int{}, n_max_sum{};
    double tol_sum;
    std::vector<double> q_grid;
    std::vector<gsl_interp_accel *> accel_lookup;
    std::vector<gsl_interp_accel *> accel_lookup_deriv;

    gsl_vector* c, *c_deriv;
    gsl_vector* u_vals, *u_deriv_vals;
    gsl_matrix *X_mat, *cov, *coeff_mat;
    gsl_matrix *X_mat_deriv, *cov_deriv, *coeff_mat_deriv;

    std::vector<gsl_spline*> coeff_splines;
    std::vector<gsl_spline*> coeff_splines_deriv;

    static double get_vec_abs_ht(const std::vector<double>& v);
    static double get_diff_vec_abs_ht(const std::vector<double>& a, const std::vector<double>& b);
    void create_deriv_lookup_table();
    void create_pair_action_table();
    double q_max;
    double q_min = 0.0;
    int P;
    double epsilon;
    double z, kappa;
};


pair_action_lookup_table::pair_action_lookup_table(double z1_, double z2_, double m1_, double m2_, double l_, double beta_, int n_s_, int n_q_,
                                                   int polydeg_, int P_, bool calc_deriv, int n_max_int_, int n_max_sum_, double tol_sum_, double lambda_max_)
                         : z1(z1_), z2(z2_), m1(m1_), m2(m2_), l(l_),
                           beta(beta_), n_s(n_s_), n_q(n_q_), polydeg(polydeg_), n_max_int(n_max_int_), P(P_), n_max_sum(n_max_sum_), tol_sum(tol_sum_), coeff_splines(polydeg_ + 1), q_grid(n_q),
                           accel_lookup(n_q_+1) , lambda_max(lambda_max_), accel_lookup_deriv(n_q + 1), coeff_splines_deriv(polydeg+1), epsilon(beta_ / (double) P_ )
{

    double mu = m1 * m2 / ( m1 + m2);
    double chisq;
    double q = 0.0;


    // set up the necessary values for the q grid
    q_max = sqrt(3.0) * l / 2;
    q_min = 0.0;
    double dq = (q_max - q_min) / (n_q - 1);

    // Get the parameters for the
    kappa = 1/(2 * mu);
    z = z1*z2 / kappa;
    lambda = sqrt(4*kappa*epsilon);



    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);
    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);
    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);
    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);
    c = gsl_vector_alloc(polydeg+1);
    cov = gsl_matrix_alloc(polydeg+1, polydeg+1);
    u_vals = gsl_vector_alloc(n_s);



    // Allocate the corresponding fields
    coeff_mat_deriv = gsl_matrix_alloc(n_q, polydeg+1);
    X_mat_deriv = gsl_matrix_alloc(n_s, polydeg + 1);
    c_deriv = gsl_vector_alloc(polydeg + 1);
    cov_deriv = gsl_matrix_alloc(polydeg + 1, polydeg +1);
    u_deriv_vals = gsl_vector_alloc(n_s);


    if(calc_deriv)
    {
        create_deriv_lookup_table();
    }
    else
    {
        create_pair_action_table();
    }
};


pair_action_lookup_table::pair_action_lookup_table(double z1_, double z2_, double m1_, double m2_, double l_, double beta_, int n_s_,
                                                   int n_q_, int P_, int polydeg_, int n_max_int_, int n_max_sum_ , double tol_sum_, double lambda_max_) :
        z1(z1_), z2(z2_), m1(m1_), m2(m2_), l(l_),
        beta(beta_), n_s(n_s_), n_q(n_q_), polydeg(polydeg_), n_max_int(n_max_int_), n_max_sum(n_max_sum_), tol_sum(tol_sum_), coeff_splines(polydeg_ + 1), q_grid(n_q),
        accel_lookup(n_q_+1) , lambda_max(lambda_max_), accel_lookup_deriv(n_q + 1), coeff_splines_deriv(polydeg+1), epsilon(beta_ / (double) P_)

{
    double mu = m1 * m2 / ( m1 + m2);
    double chisq;
    double q = 0.0;


    // set up the necessary values for the q grid
    q_max = sqrt(3.0) * l / 2;
    q_min = 0.0;
    double dq = (q_max - q_min) / (n_q - 1);

    // Get the parameters for the
    kappa = 1/(2 * mu);
    z = z1*z2 / kappa;
    lambda = sqrt(4*kappa*epsilon);


    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);
    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);
    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);
    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);
    c = gsl_vector_alloc(polydeg+1);
    cov = gsl_matrix_alloc(polydeg+1, polydeg+1);
    u_vals = gsl_vector_alloc(n_s);


    // Allocate the corresponding fields
    coeff_mat_deriv = gsl_matrix_alloc(n_q, polydeg+1);
    X_mat_deriv = gsl_matrix_alloc(n_s, polydeg + 1);
    c_deriv = gsl_vector_alloc(polydeg + 1);
    cov_deriv = gsl_matrix_alloc(polydeg + 1, polydeg +1);
    u_deriv_vals = gsl_vector_alloc(n_s);


    create_pair_action_table();
}


pair_action_lookup_table::pair_action_lookup_table(const std::string &filename, const std::string &filename_deriv, double z1_, double z2_, double m1_, double m2_,
                                                   double l_, double beta_, int n_s_, int n_q_, int polydeg_, int P_, int n_max_int_,
                                                   int n_max_sum_, double tol_sum_, double lambda_max_) :
        z1(z1_), z2(z2_), m1(m1_), m2(m2_), l(l_),
        beta(beta_), n_s(n_s_), n_q(n_q_), polydeg(polydeg_), n_max_int(n_max_int_), n_max_sum(n_max_sum_), tol_sum(tol_sum_), coeff_splines(polydeg_ + 1), q_grid(n_q),
        accel_lookup(n_q_+1) , lambda_max(lambda_max_), accel_lookup_deriv(n_q + 1), coeff_splines_deriv(polydeg+1), epsilon(beta_ / (double) P_)
{
    /**
     * This constructor reallocates a previously calculated lookup-table by reading two files given by the name filename filename_deriv.
     */

    std::ifstream file(filename);
    std::ifstream file_deriv(filename_deriv);
    if(!file.is_open()) throw std::runtime_error("Could not open coefficients File!\n");
    if(!file_deriv.is_open()) throw std::runtime_error("Could not open derivative coefficients File!\n");


    // Initialize the data-structures
    double mu = m1 * m2 / ( m1 + m2);
    kappa = 1/(2 * mu);
    z = z1*z2 / kappa;
    lambda = sqrt(4*kappa*epsilon);
    // set up the necessary values for the q grid
    double q = 0.0;
    q_max = sqrt(3.0) * l / 2;
    q_min = 0.0;
    double dq = (q_max - q_min) / (n_q - 1);

    auto fitting_ws = gsl_multifit_linear_alloc(n_s, polydeg+1);

    double coeff_val;

    int file_line = 0;
    std::string line;


    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);
    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);
    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);
    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);
    c = gsl_vector_alloc(polydeg+1);
    cov = gsl_matrix_alloc(polydeg+1, polydeg+1);
    u_vals = gsl_vector_alloc(n_s);

    // Allocate the corresponding fields
    coeff_mat_deriv = gsl_matrix_alloc(n_q, polydeg+1);
    X_mat_deriv = gsl_matrix_alloc(n_s, polydeg + 1);
    c_deriv = gsl_vector_alloc(polydeg + 1);
    cov_deriv = gsl_matrix_alloc(polydeg + 1, polydeg +1);
    u_deriv_vals = gsl_vector_alloc(n_s);


    file_line = 0;

    // The file only contains floating point values and thus we now read everything line-by-line
    // Iterate over all n_q lines of the file
    for(int ln = 0; ln < n_q; ln++)
    {

        // read the next line in the file
        std::getline(file, line);

        // create an inputstringstream
        std::istringstream  in(line);

        for(int col = 0; col <= polydeg+1; col++)
        {
            // Read all the double values in each line
            in >> coeff_val;

            // The first column contains the q-values and thus have to be assinged to the q_g
            if(col == 0)
            {
                q_grid[ln] = coeff_val;
            }
            else
            {
                gsl_matrix_set(coeff_mat, ln, col-1, coeff_val);
            }
        }
        ++file_line;
    }
    file.close();


    // The coefficient parsing is now finished and now the interpolation
    for(int p = 0; p <= polydeg; p++)
    {
        auto vals = get_coeff_mat_column(p);
        accel_lookup[p] = gsl_interp_accel_alloc();
        coeff_splines[p] = gsl_spline_alloc(gsl_interp_cspline, n_q);
        auto err_code = gsl_spline_init((coeff_splines[p]), q_grid.data(), vals.data(), n_q);
        if(err_code)
        {
            std::cout << "Error! Interpolation failed with code: " << gsl_strerror(err_code) << std::endl;
            exit(2);
        }
    }


    file_line = 0.0;

    // The file only contains floating point values, and thus we now read everything line-by-line
    // Iterate over all n_q lines of the file
    for(int ln = 0; ln < n_q; ln++)
    {

        // read the next line in the file
        std::getline(file_deriv, line);

        // create an inputstringstream
        std::istringstream  in(line);

        for(int col = 0; col <= polydeg+1; col++)
        {
            // Read all the double values in each line
            in >> coeff_val;

            // The first column contains the q-values and thus have to be assinged to the q_g
            if(col == 0)
            {
                q_grid[ln] = coeff_val;
            }
            else
            {
                gsl_matrix_set(coeff_mat_deriv, ln, col-1, coeff_val);
            }
        }
        ++file_line;
    }
    file_deriv.close();


    // The coefficient parsing is now finished and now the interpolation
    for(int p = 0; p <= polydeg; p++)
    {
        auto vals = get_coeff_deriv_mat_columns(p);
        accel_lookup_deriv[p] = gsl_interp_accel_alloc();
        coeff_splines_deriv[p] = gsl_spline_alloc(gsl_interp_cspline, n_q);
        auto err_code = gsl_spline_init((coeff_splines_deriv[p]), q_grid.data(), vals.data(), n_q);
        if(err_code)
        {
            std::cout << "Error! Interpolation failed with code: " << gsl_strerror(err_code) << std::endl;
            exit(2);
        }
    }

}


double pair_action_lookup_table::calc_pair_action(std::vector<double> &a, std::vector<double>& b)
{
    auto ra = get_vec_abs_ht(a);
    auto rb = get_vec_abs_ht(b);

    auto q = 0.5 * (ra + rb);
    auto s = get_diff_vec_abs_ht(a, b);
    if(q > q_max || q >= lambda_max * lambda) return calc_u_prim(ra, rb, z, kappa) * epsilon;
    auto u_pair = interpolate(q, s);
    auto u_prim = calc_u_prim(ra, rb, z, kappa) * epsilon;

    auto du_prim_pair = abs(u_pair - u_prim);

    if(u_pair < 0.0)
    {
        std::string neg_fname("neg_exp_vals.csv");
        print_off_diag_expansion(q, 500, neg_fname);
        double rho1 = rho_coulomb_qs(q, s, epsilon, kappa, z, n_max_int, n_max_sum, tol_sum);
        double u_free = u_free_qs(s, kappa, epsilon);

        u_pair = u_free - log(rho1);

        if(u_pair > 0.0)
        {
            return u_pair;
        }
        else
        {
            return u_prim;
        }
    }


    if(abs(u_pair) < 1e-3)
    {
        return u_prim;
    }

    if(q < lambda)
    {
        return u_pair;
    }
    else
    {
        if(q > lambda_max * lambda)
        {
            return u_prim;
        }
        // Check how well u_pair and u_prim are in agreement.
        if( abs(u_prim - u_pair) < 1e-4)
        {
            return u_prim;
        }
        else
        {
            return u_pair;
        }
    }
}


double pair_action_lookup_table::calc_pair_action_deriv(std::vector<double> &a, std::vector<double> &b)
{
    auto ra = get_vec_abs_ht(a);
    auto rb = get_vec_abs_ht(b);
    auto q = 0.5 * (ra + rb);
    auto s = get_diff_vec_abs_ht(a, b);

    auto u_pair_deriv = interpolate_deriv(q, s);
    auto u_prim_deriv = u_beta_deriv_prim_approx(ra, rb, P, kappa, z);
    if(q == 0.0 ) return u_pair_deriv;
    if(q > q_max || q >= lambda_max * lambda) return u_prim_deriv;


    if(abs(u_pair_deriv) < 1e-3)
    {
        return u_prim_deriv;
    }

    if(q < lambda)
    {
        return u_pair_deriv;
    }
    else
    {
        if(q > lambda_max * lambda)
        {
            return u_prim_deriv;
        }
        else
        {
            return u_pair_deriv;
        }
    }

};


void pair_action_lookup_table::print_coefficient_interpolation(int c_id, std::string& fname)
{
    std::ofstream coeff_file;
    coeff_file.open(fname.c_str());
    coeff_file << "# q, A_"<<c_id << "\n";

    for(int i = 0; i < n_q; i++)
    {
        coeff_file << gsl_matrix_get(coeff_mat, i, c_id);
    }
}


void pair_action_lookup_table::print_off_diag_expansion(double q, int num_s, std::string& fname)
{
    /**
     * This function prints the off-diagonal expansion for a ceratin value of q to a file in the
     * range of [s_min,s_max] with num_s points
     */

    // set up the necessary values for the s grid
    double s = 0.0;

    double s_max = 0.0;
    double wave_lim = lambda_max * lambda;
    double slim_up = 2 * q;

    if(slim_up < wave_lim)
    {
         s_max = slim_up;
    }
    else
    {
        s_max = wave_lim;
    }
    const double s_min = 0.0;
    double ds = (s_max - s_min) / (num_s);

    std::ofstream od_data_file;
    od_data_file.open(fname.c_str());

    od_data_file << "# s u(q,s)[expansion] u(q,s)[formula] \n";
    for(int i = 0; i < num_s; i++)
    {
        s = s_min + ds * i;

        auto val_interp = interpolate(q, s);
        auto val_form = -log(rho_coulomb_qs(q, s, epsilon, kappa, z, n_max_int, n_max_sum, tol_sum) / rho_free_qs(s, kappa, epsilon));



        od_data_file << s << ", " << val_interp << ", " << val_form << "\n";

    }
    std::cout << "Finished printing off-diagonal expansion" << std::endl;
    od_data_file.close();
}


void pair_action_lookup_table::print_off_diag_expansion_deriv(double q, int num_s, std::string &fname)
{

    // set up the necessary values for the s grid
    double s = 0.0;

    double s_max = 0.0;
    double wave_lim = lambda_max * lambda;
    double slim_up = 2 * q;

    if(slim_up < wave_lim)
    {
        s_max = slim_up;
    }
    else
    {
        s_max = wave_lim;
    }
    const double s_min = 0.0;
    double ds = (s_max - s_min) / (num_s);

    std::ofstream od_data_file;
    od_data_file.open(fname.c_str());

    od_data_file << "# s u(q,s)'[expansion] u(q,s)[formula] \n";


    double delta_beta = 1e-6;
    for(int i = 0; i < num_s; i++)
    {


        auto u_t1 = calc_u_tot(q, s, beta , kappa/P, z, 5000, 1200, 1e-12);
        auto u_t2 = calc_u_tot(q, s, beta  + delta_beta, kappa/P, z, 5000, 1200, 1e-12);
        auto u_deriv_num = (u_t2 - u_t1) / delta_beta;

        s = s_min + ds * i;

        auto val_interp = interpolate_deriv(q, s);
        auto val_an = u_beta_deriv(q, s, beta, P, kappa, z);

        od_data_file << s << ", " << val_interp <<", " << val_an << ", " << u_deriv_num << "\n";

    }
    std::cout << "Finished printing off-diagonal expansion" << std::endl;
    od_data_file.close();
}

void pair_action_lookup_table::print_diagonal_interpolation(int num_points, std::string& fname)
{
    /**
     * This function prints the diagonal element interpolation vs the analytical formula into a .csv file for debug purposes.
     * It requires the number of points between q_min and q_max
     */

    // set up the necessary values for the q grid
    double q = 0.0;
    double dq = (q_max - q_min) / (num_points - 1);

    std::ofstream data_file;
    data_file.open(fname.c_str());

    data_file << "# q u(q,0) [interpolated]  u(q,0) [analytical result] \n";
    for(int i = 0; i < num_points; i++)
    {
        q = q_min + i * dq;
        auto val_interp = interpolate(q, 0.0);
        auto val_formula = -log(rho_coulomb_qs(q, 0.0, epsilon, kappa, z, n_max_int, n_max_sum, tol_sum)/rho_free_qs(0.0, kappa, epsilon));

        data_file << q << ", " << val_interp << ", " << val_formula << "\n";


    }

    std::cout << "Finished diagonal printing! \n";
    data_file.close();
}



double pair_action_lookup_table::interpolate(double q, double s)
{
    double val = 0;

    if(s==0)
    {
        val = gsl_spline_eval(coeff_splines[0], q, accel_lookup[0]);
        return val;
    }

    if(s >= 2*q )
    {
        std::cout << "Error! s > 2*q. This violates the triangle inequality" << std::endl;
        assert(s < 2* q);
    }

    if(q < 0.001 && epsilon < 1.0)
    {
        return u_cusp_qs(q, s, epsilon, z, kappa);
    }


    for(int i = 0; i <= polydeg; i++)
    {
        val += gsl_spline_eval(coeff_splines[i], q, accel_lookup[i]) * std::pow(std::pow(s,2),i);

    }

    return val;
}


double pair_action_lookup_table::interpolate_deriv(double q, double s)
{
    double val = 0.0;


    if(s > 2*q )
    {
        std::cout << "Error! s > 2*q. This violates the triangle inequality" << std::endl;
        assert(s < 2* q);
    }

    if(s==0)
    {
        val = gsl_spline_eval(coeff_splines_deriv[0], q, accel_lookup_deriv[0]);
        return val;
    }

    for(int i = 0; i <= polydeg; i++)
    {
        val += gsl_spline_eval(coeff_splines_deriv[i], q, accel_lookup_deriv[i]) * std::pow(std::pow(s,2),i);

    }

    return val;
}


void pair_action_lookup_table::print_coeff_mat()
{
    std::cout << "|--- Coefficient Matrix ---| \n";

    for(int i = 0; i < n_q; i++)
    {
        for(int j = 0; j <= polydeg; j++)
        {
            std::cout << " " << gsl_matrix_get(coeff_mat, i, j) << " ";
        }
        std::cout << "\n";
    }

    std::cout << "|------------------------| \n";
}


std::vector<double> pair_action_lookup_table::get_coeff_mat_column(int idx)
{
    std::vector<double> coeff_col(n_q);

    for(int i = 0; i < n_q; i++)
    {
        coeff_col[i] = gsl_matrix_get(coeff_mat, i, idx);
    }

    return coeff_col;
}


std::vector<double> pair_action_lookup_table::get_coeff_deriv_mat_columns(int idx)
{

    std::vector<double> coeff_col(n_q);

    for(int i = 0; i < n_q; i++)
    {
        coeff_col[i] = gsl_matrix_get(coeff_mat_deriv, i, idx);
    }

    return coeff_col;
}


pair_action_lookup_table::~pair_action_lookup_table()
{

    // First the pair-action fields
    gsl_matrix_free(X_mat);
    gsl_matrix_free(cov);
    gsl_matrix_free(coeff_mat);
    gsl_vector_free(c);
    gsl_vector_free(u_vals);


    // Next the derivative fields
    gsl_matrix_free(coeff_mat_deriv);
    gsl_matrix_free(X_mat_deriv);
    gsl_matrix_free(cov_deriv);
    gsl_vector_free(c_deriv);
    gsl_vector_free(u_deriv_vals);


    for(auto&& i: coeff_splines)
    {
        gsl_spline_free(i);
    }

    for(auto&& j: accel_lookup)
    {
        gsl_interp_accel_free(j);
    }


}


void pair_action_lookup_table::print_interpolation_table(std::string &fname)
{
    std::ofstream table;
    table.open(fname.c_str());

    // set up the necessary values for the q grid
    double q = 0.0;

    double dq = (q_max - q_min) / (n_q - 1);

    for(int i = 0; i < n_q; i++)
    {
        q = i * dq + q_min;

        table << std::fixed << std::setprecision(12) << q <<" ";
        for(int j = 0; j <= polydeg; j++)
        {
            if(j!= polydeg)
            {
                table << std::fixed << std::setprecision(12) << gsl_matrix_get(coeff_mat, i, j) << " ";
            }
            else
            {
                table << std::fixed << std::setprecision(12) << gsl_matrix_get(coeff_mat, i, j);
            }
        }
        table << "\n";
    }
}


double pair_action_lookup_table::get_vec_abs_ht(const std::vector<double> &v)
{
    double result = {0.0};

    for(auto&& i : v)
    {
        result += i*i;
    }

    return sqrt(result);
}


double pair_action_lookup_table::get_diff_vec_abs_ht(const std::vector<double> &a, const std::vector<double> &b)
{
    double result = {0.0};

    auto range = a.size();

    for(auto i = 0; i < range; i++)
    {
        result += (a[i] - b[i]) * (a[i] - b[i]);
    }

    result = std::sqrt(result);

    return result;

}


// This function may be called only after the constructor initilized the necessary fields
void pair_action_lookup_table::create_deriv_lookup_table()
{

    auto fitting_ws_deriv = gsl_multifit_linear_alloc(n_s, polydeg+1);

    std::ofstream chi_sq_deriv_file;
    if(z < 0)
    {
        chi_sq_deriv_file.open("chi_diagnostic_deriv-e-p.csv");
    }

    if(z > 0)
    {
        chi_sq_deriv_file.open("chi_diagnostic_deriv-e-e.csv");
    }

    std::cout << "Creating pair-action derivative lookup table" << std::endl;
    double dq = (q_max - q_min) / (n_q - 1);

    double q = 0.0;
    float prog = 0.0;

    double chi_sq_deriv;

    for(int i = 0; i < n_q; i++)
    {
        prog = (float) i / (float) n_q;
        int barWidth = 50;
        int pos = prog * barWidth;
        std::cout << "[";

        for(int bar = 0; bar < barWidth; ++bar)
        {
            if(bar < pos) std::cout << "=";
            else if (bar == pos) std::cout << ">";
            else std::cout << " ";
        }
        std::cout << "]" << int(prog * 100.0) << "% \r" << std::flush;

        q = i * dq + q_min;
        // set up the necessary values for the s grid
        double s = 0.0;
        double s_max = 0.0;
        double wave_lim = lambda_max * lambda;
        double slim_up = 2 * q;

        if(slim_up < wave_lim)
        {
            s_max = slim_up;
        }
        else
        {
            s_max = wave_lim;
        }
        const double s_min = 0.0;
        double ds = (s_max - s_min) / (n_s);

        auto u_diag_deriv = u_beta_deriv(q, 0.0, beta, P, kappa, z);
        if(isnan(u_diag_deriv))
        {
            std::cout << "Error received NaN from rho coulomb \n";
            assert(!isnan(u_diag_deriv));
        }

        q_grid[i] = q;

        for(int j = 0; j < n_s; j++)
        {
            // Generate the necessary values to obtain the potential action derivative
            s = j * ds + s_min;
            double u_deriv = u_beta_deriv(q, s, beta, P, kappa, z);

            double du_deriv = u_deriv - u_diag_deriv;

            if(isnan(u_deriv) || isnan(u_diag_deriv))
            {
                std::cout << "Error! Received NaN as answer for du \n";
                assert(!(isnan(u_deriv) || isnan(du_deriv)));
            }

            gsl_vector_set(u_deriv_vals, j, du_deriv);

            gsl_matrix_set(X_mat_deriv, j, 0, 0.0);

            for(int k = 1; k <= polydeg;  k++)
            {
                gsl_matrix_set(X_mat_deriv, j, k, pow(pow(s, 2), k));
            }

        }

        gsl_multifit_linear(X_mat_deriv, u_deriv_vals, c_deriv, cov_deriv, &chi_sq_deriv,fitting_ws_deriv);

        gsl_matrix_set(coeff_mat_deriv, i, 0, u_diag_deriv);

        chi_sq_deriv_file << q << ", " << chi_sq_deriv << "\n";

        for(int n = 1; n <= polydeg; n++)
        {
            gsl_matrix_set(coeff_mat_deriv, i, n, gsl_vector_get(c_deriv, n));
        }

    }

    std::cout << "\n";

    // Create the fit
    for(int p = 0; p <= polydeg; p++)
    {
        auto vals = get_coeff_deriv_mat_columns(p);
        accel_lookup_deriv[p] = gsl_interp_accel_alloc();
        coeff_splines_deriv[p] = gsl_spline_alloc(gsl_interp_cspline, n_q);
        auto err_code = gsl_spline_init((coeff_splines_deriv[p]), q_grid.data(), vals.data(), n_q);
        if(err_code)
        {
            std::cout << "Error! Interpolation failed with code: " << gsl_strerror(err_code) << std::endl;
            exit(2);
        }

    }


    std::cout << "Fitting and interpolation of the derivative table finished!" << std::endl;
    chi_sq_deriv_file.close();
    std::string table_file_name = "pair_action_deriv_coefficients.csv";
    print_deriv_interpolation_table(table_file_name);

}


void pair_action_lookup_table::create_pair_action_table()
{

    // Define the necessary fitting parameters
    // assuming constructor initialized all the necessary fields before
    double mu = m1 * m2 / ( m1 + m2);
    double chisq;
    double q = 0.0;
    double dq = (q_max - q_min) / (n_q - 1);




    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);

    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);

    auto fitting_ws = gsl_multifit_linear_alloc(n_s, polydeg+1);
    c = gsl_vector_alloc(polydeg+1);
    cov = gsl_matrix_alloc(polydeg+1, polydeg+1);
    u_vals = gsl_vector_alloc(n_s);


    std::ofstream chi_sq_file;
    if(z < 0)
    {
        chi_sq_file.open("chi_diagnostic-e-p.csv");
    }

    if(z > 0)
    {
        chi_sq_file.open("chi_diagnostic-e-e.csv");
    }


    std::cout << "Creating pair-action Lookup table " << std::endl;
    float prog = 0.0;
    for(int i = 0; i < n_q; i++)
    {

        prog = (float) i / (float) n_q;
        int barWidth = 50;
        int pos = prog * barWidth;
        std::cout << "[";

        for(int bar = 0; bar < barWidth; ++bar)
        {
            if(bar < pos) std::cout << "=";
            else if (bar == pos) std::cout << ">";
            else std::cout << " ";
        }
        std::cout << "]" << int(prog * 100.0) << "% \r" << std::flush;


        q = i*dq + q_min;
        // set up the necessary values for the s grid
        double s = 0.0;
        double s_max = 0.0;
        double wave_lim = lambda_max * lambda;
        double slim_up = 2 * q;

        if(slim_up < wave_lim)
        {
            s_max = slim_up;
        }
        else
        {
            s_max = wave_lim;
        }
        const double s_min = 0.0;
        double ds = (s_max - s_min) / (n_s);

        auto u_diag = u_free_qs(s, kappa, epsilon) - log(rho_coulomb_qs(q, 0, epsilon, kappa, z, n_max_int, n_max_sum, tol_sum));

        if(isnan(u_diag))
        {
            std::cout << "Error received NaN from rho coulomb \n";
            assert(!isnan(u_diag));
        }

        q_grid[i] = q;

        for(int j = 0; j < n_s; j++)
        {
            // Generate the values of the potential action for a constant q value
            s = j * ds + s_min;
            // We are fitting the difference of the off-diagonal and diagonal element to a polynomial, thus the "observed" values have this form:
            double rho1 = rho_coulomb_qs(q, s, epsilon, kappa, z, n_max_int, n_max_sum, tol_sum);
            double rhof = rho_free_qs(s, kappa, epsilon);
            double ufree = u_free_qs(s, kappa, epsilon);
            double u = ufree - log(rho1);

            double du = u - u_diag;

            if(isnan(rho1) || isnan(du) || rho1 < 0.0)
            {
                std::cout << "Error! Received NaN as answer for du \n";
                std::cout << "s = " << s <<", q = " << q << ", epsilon = " << epsilon <<", kappa = " << kappa << ", z = " << z <<" \n";
                std::cout << "n_max_int = " << n_max_int << ", n_max_sum = " << n_max_sum << ", tol_sum = " << tol_sum << "\n";
                std::cout << "Rho1 = " << rho1 << ", rhof = " << rhof << ", u = " << u << ", du = " << du  <<"\n";
                assert(!(isnan(rho1) || isnan(du)));
            }
            gsl_vector_set(u_vals, j, du);

            // Now we iterate over all columns to build up the observation matrix

            // There will be no zeroth order in the fitting procedure
            gsl_matrix_set(X_mat, j, 0, 0.0);


            for(int k = 1; k <= polydeg; k++)
            {
                // otherwise add the entry (s^2)^k
                gsl_matrix_set(X_mat, j, k, pow(pow(s,2),k));
            }
        }


        // Now obtain the fitting coefficients through linear regression
        gsl_multifit_linear(X_mat, u_vals, c, cov, &chisq, fitting_ws);

        // The u(q,0) coefficient has to be set manually.
        gsl_matrix_set(coeff_mat, i, 0, u_diag);

        chi_sq_file << q <<", " << chisq << "\n";
        for(int n = 1; n <= polydeg; n++)
        {
            gsl_matrix_set(coeff_mat, i, n, gsl_vector_get(c,n));
        }


    }

    std::cout << "\n";

    gsl_multifit_linear_free(fitting_ws);


    // The fitting procedure is now finished and now the interpolation
    for(int p = 0; p <= polydeg; p++)
    {
        auto vals = get_coeff_mat_column(p);
        accel_lookup[p] = gsl_interp_accel_alloc();
        coeff_splines[p] = gsl_spline_alloc(gsl_interp_cspline, n_q);
        auto err_code = gsl_spline_init((coeff_splines[p]), q_grid.data(), vals.data(), n_q);
        if(err_code)
        {
            std::cout << "Error! Interpolation failed with code: " << gsl_strerror(err_code) << std::endl;
            exit(2);
        }
    }

    std::cout << "Fitting and interpolation finished!" << std::endl;
    chi_sq_file.close();
    std::string table_file_name = "pair_action_coefficients.csv";
    print_interpolation_table(table_file_name);

}


void pair_action_lookup_table::print_deriv_interpolation_table(std::string &fname)
{
    std::ofstream table;
    table.open(fname.c_str());

    // set up the necessary values for the q grid
    double q = 0.0;

    double dq = (q_max - q_min) / (n_q - 1);

    // Print all the Meta-Data into several comments lines

    //table << "# epsilon = " << beta << ", z= Z_1*Z_2/kappa = " << z << ", kappa = 1/(2*mu) = " << kappa << ", mu = " << m1 * m2 / (m1 + m2) << "\n";
    //table << "# n_q = " << n_q << ", n_s = " << n_s << ", dq = " << dq << ", length = " << l << ", lambda = " << lambda << ", lambda_max = " << lambda_max <<", Polydeg = "<< polydeg << "\n";

    for(int i = 0; i < n_q; i++)
    {
        q = i * dq + q_min;

        table << std::fixed << std::setprecision(12) << q <<" ";
        for(int j = 0; j <= polydeg; j++)
        {
            if(j!= polydeg)
            {
                table << std::fixed << std::setprecision(12) << gsl_matrix_get(coeff_mat_deriv, i, j) << " ";
            }
            else
            {
                table << std::fixed << std::setprecision(12) << gsl_matrix_get(coeff_mat_deriv, i, j);
            }
        }
        table << "\n";
    }

}


std::vector<double> pair_action_lookup_table::get_pair_action(std::vector<double>& a, std::vector<double>& b)
{


    double pair_pot = calc_pair_action(a, b);
    double pair_action_deriv = calc_pair_action_deriv(a, b);
    std::vector<double> res = {pair_pot, pair_action_deriv};
    return res;
}
