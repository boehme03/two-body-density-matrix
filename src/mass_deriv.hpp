#pragma once

//
// Created by boehme03 on 08.12.23.
//

#include <cstdlib>
#include <type_traits>


#include "density_mat_qs.hpp"
#include "density_mat_qs_beta_deriv.hpp"

class mass_deriv
{

public:

    // Construct the mass lookup table for a given q,s parameter space and masses.
    //
    mass_deriv(double z1_, double z2_, double m1_, double m2_, double l_, double beta_, int n_s_, int n_q_,
               int polydeg_, int P_, int n_max_int_ = 5000, int n_max_sum_ = 1500, double tol_sum_ = 1e-12, double lambda_max_ = 3.0);

    double get_uqs_mass(double  q, double s, double mp1, double mp2) const;
    double get_deriv_convergence(int spec, double tol_rel=1e-3);
    void create_mass_deriv_electron_lookup_table();
    void create_mass_deriv_ion_lookup_table();

private:

    double z1, z2, m1, m2, l, beta;
    int n_s, n_q, polydeg, n_max_int{}, n_max_sum{};
    int P;
    double tol_sum;
    double lambda_max;

    double m_pert;


    std::vector<double> q_grid;
    std::vector<gsl_interp_accel *> accel_lookup;
    std::vector<gsl_interp_accel *> accel_lookup_deriv;

    gsl_vector* c, *c_deriv;
    gsl_vector* u_vals, *u_deriv_vals;
    gsl_matrix *X_mat, *cov, *coeff_mat;
    gsl_matrix *X_mat_deriv, *cov_deriv, *coeff_mat_deriv;

    std::vector<gsl_spline*> coeff_splines;
    std::vector<gsl_spline*> coeff_splines_deriv;


    static double get_vec_abs_ht(const std::vector<double>& v);
    static double get_diff_vec_abs_ht(const std::vector<double>& a, const std::vector<double>& b);

    double q_max;
    double q_min = 0.0;

    double epsilon;
    double z, kappa;

    // undisturbed kappa, wavelength and z0
    double z0, kappa0, lambda0;

    double deltam_e = 0.0;


    std::vector<double> get_coeff_mat_column(int idx);

    void print_coefficient_interpolation(int c_id, std::string &fname);

    void print_interpolation_table(std::string &fname);
};

mass_deriv::mass_deriv(double z1_, double z2_, double m1_, double m2_, double l_, double beta_, int n_s_, int n_q_,
                       int polydeg_, int P_, int n_max_int_, int n_max_sum_, double tol_sum_, double lambda_max_)
        : z1(z1_), z2(z2_), m1(m1_), m2(m2_), l(l_),
          beta(beta_), n_s(n_s_), n_q(n_q_), polydeg(polydeg_), n_max_int(n_max_int_), P(P_), n_max_sum(n_max_sum_), tol_sum(tol_sum_), coeff_splines(polydeg_ + 1), q_grid(n_q),
          accel_lookup(n_q_+1) , lambda_max(lambda_max_), accel_lookup_deriv(n_q + 1), coeff_splines_deriv(polydeg+1), epsilon(beta_ / (double) P_ )
{

    double mu0 = m1 * m2 / (m1 + m2);
    double chisq;
    double q = 0.0;

    q_max = sqrt(3.0) * l / 2;
    q_min = 0.0;

    double dq = (q_max - q_min) / (n_q - 1);

    kappa0 = 1 / (2 * mu0);
    z0 = z1 * z2 / kappa0;

    // undisturbed mass density
    lambda0  = sqrt(4 * kappa0 * epsilon);


    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);
    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);
    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);
    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);
    c = gsl_vector_alloc(polydeg+1);
    cov = gsl_matrix_alloc(polydeg+1, polydeg+1);
    u_vals = gsl_vector_alloc(n_s);



    // Allocate the corresponding fields
    coeff_mat_deriv = gsl_matrix_alloc(n_q, polydeg+1);
    X_mat_deriv = gsl_matrix_alloc(n_s, polydeg + 1);
    c_deriv = gsl_vector_alloc(polydeg + 1);
    cov_deriv = gsl_matrix_alloc(polydeg + 1, polydeg +1);
    u_deriv_vals = gsl_vector_alloc(n_s);





}

double mass_deriv::get_vec_abs_ht(const std::vector<double> &v)
{
    double result = {0.0};

    for(auto&& i : v)
    {
        result += i*i;
    }

    return sqrt(result);
}


double mass_deriv::get_diff_vec_abs_ht(const std::vector<double> &a, const std::vector<double> &b)
{
    double result = {0.0};

    auto range = a.size();

    for(auto i = 0; i < range; i++)
    {
        result += (a[i] - b[i]) * (a[i] - b[i]);
    }

    result = std::sqrt(result);

    return result;

}

double mass_deriv::get_deriv_convergence(int spec, double tol_rel)
{

    /**
     * The forward differentiation includes the error behaviour O(h). This means the smaller the perturbation,
     * the more accurate the numerical derivative will be. To achieve a derivative as precise as possible, we use
     * the rule of the unit in the last place (ULP): h = x0 * sqrt(2*eps).
     *
     * Species convention:
     *                      m1 : Electrons
     *                      m2 : Ions
     *
     * Make sure this is correct in the config files!
     */


    constexpr double eps = std::numeric_limits<double>::epsilon();


    double deltam = sqrt(2 * eps);
    if(spec == 1 )
    {
        deltam *= m1;
    }
    if(spec == 2)
    {
        deltam *= m2;
    }
    if(spec != 1 && spec != 2)
    {
        std::cout << "Error! Only species 1 and 2 to choose from for a differentiation step size! " << std::endl;
        std::cout << "Convention: m_1 : electrons, m2 : ions. Make sure this is is correclty specified in the table.cfg file!" << std::endl;
    }

    std::cout << "Numerical Epsilon for the given machine is: " << eps << std::endl;
    std::cout << "Setting numerical derivative to m_"<< spec <<"*sqrt(2 * eps) =  " << deltam << std::endl;
    std::cout << "Using the given numerical accuracy for building the derivative." << std::endl;

    return deltam;
}

double mass_deriv::get_uqs_mass(double q, double s, double mp1, double mp2) const
{
    /**
     * This is a small wrapper function for the mass derivative.
     * The addition of p means "perturbed".
     */

    double mup = ( mp1 * mp2 ) / (mp1 + mp2);
    double kappap = 1. / (2.0 * mup);
    double zp = z1 * z2 / kappap;

    return calc_u_tot(q, s, beta, kappap, zp, 5000, 1500, 1e-12);
}


void mass_deriv::create_mass_deriv_electron_lookup_table()
{

    /**
     * Function to calculate and create the mass derivative of the density matrix from a finite
     * difference scheme.
     */

    // Define the necessary fitting parameters
    // assuming constructor initialized all the necessary fields before
    double mu = m1 * m2 / ( m1 + m2);
    double chisq;
    double q = 0.0;
    double dq = (q_max - q_min) / (n_q - 1);



    // Use parameter 2 as this is done here for electrons
    double deltam = get_deriv_convergence(1);


    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);

    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);

    auto fitting_ws = gsl_multifit_linear_alloc(n_s, polydeg+1);
    c = gsl_vector_alloc(polydeg+1);
    cov = gsl_matrix_alloc(polydeg+1, polydeg+1);
    u_vals = gsl_vector_alloc(n_s);

    std::ofstream chi_sq_file;

    chi_sq_file.open("chi_diagnostic-mass_derivative.csv");

    std::cout << "Creating pair-action derivative lookup table for the mass derivative of the electrons!" << std::endl;

    float prog = 0.0;

    for(int i = 0; i < n_q; i++)
    {
        prog = (float ) i / (float) n_q;
        int barWidth = 100;
        int pos = prog * barWidth;

        std::cout << "[";

        for(int bar = 0; bar < barWidth; ++bar)
        {
            if(bar < pos) std::cout << "=";
            else if (bar == pos) std::cout << ">";
            else std::cout << " ";
        }
        std::cout << "]" << int(prog * 100.0) << "% \r" << std::flush;

        q = i*dq + q_min;
        // set up the necessary values for the s grid
        double s = 0.0;
        double s_max = 0.0;
        double wave_lim = lambda_max * lambda0;
        double slim_up = 2 * q;

        if(slim_up < wave_lim)
        {
            s_max = slim_up;
        }
        else
        {
            s_max = wave_lim;
        }
        const double s_min = 0.0;
        double ds = (s_max - s_min) / (n_s);

        auto deriv_m_diag = ( get_uqs_mass(q, s, m1 + deltam, m2 )- get_uqs_mass(q, s, m1, m2) )
                                    / deltam;

        if(isnan(deriv_m_diag))
        {
            std::cout << "Error received NaN from the forward differentiation! \n";
            assert(!isnan(deriv_m_diag));
        }

        q_grid[i] = q;

        for(int j = 0; j < n_s; j++)
        {
            s = j * ds + s_min;
            auto deriv_m_od =  ( get_uqs_mass(q, s, m1 + deltam, m2 )- get_uqs_mass(q, s, m1, m2) )
                                / deltam;

            if(isnan(deriv_m_diag))
            {
                std::cout << "Error received NaN from the forward differentiation! \n";
                assert(!isnan(deriv_m_od));
            }

            double du = deriv_m_od - deriv_m_diag;
            gsl_vector_set(u_vals, j, du);

            // Now we iterate over all columns to build up the observation matrix

            // There will be no zeroth order in the fitting procedure
            gsl_matrix_set(X_mat, j, 0, 0.0);

            for(int k = 1; k <= polydeg; k++)
            {
                // otherwise add the entry (s^2)^k
                gsl_matrix_set(X_mat, j, k, pow(pow(s,2),k));
            }


        }

        // Now obtain the fitting coefficients through linear regression
        gsl_multifit_linear(X_mat, u_vals, c, cov, &chisq, fitting_ws);

        // The u(q,0) coefficient has to be set manually.
        gsl_matrix_set(coeff_mat, i, 0, deriv_m_diag);

        chi_sq_file << q <<", " << chisq << "\n";
        for(int n = 1; n <= polydeg; n++)
        {
            gsl_matrix_set(coeff_mat, i, n, gsl_vector_get(c,n));
        }
    }


    std::cout << "\n";

    gsl_multifit_linear_free(fitting_ws);


    // The fitting procedure is now finished and now the interpolation
    for(int p = 0; p <= polydeg; p++)
    {
        auto vals = get_coeff_mat_column(p);
        accel_lookup[p] = gsl_interp_accel_alloc();
        coeff_splines[p] = gsl_spline_alloc(gsl_interp_cspline, n_q);
        auto err_code = gsl_spline_init((coeff_splines[p]), q_grid.data(), vals.data(), n_q);
        if(err_code)
        {
            std::cout << "Error! Interpolation failed with code: " << gsl_strerror(err_code) << std::endl;
            exit(2);
        }
    }

    std::cout << "Fitting and interpolation finished!" << std::endl;
    chi_sq_file.close();
    std::string table_file_name = "e_pair_action_mass_deriv.csv";
    print_interpolation_table(table_file_name);



}


std::vector<double> mass_deriv::get_coeff_mat_column(int idx)
{
    std::vector<double> coeff_col(n_q);

    for(int i = 0; i < n_q; i++)
    {
        coeff_col[i] = gsl_matrix_get(coeff_mat, i, idx);
    }

    return coeff_col;
}



void mass_deriv::create_mass_deriv_ion_lookup_table()
{
    /**
     * Function to calculate and create the mass derivative of the density matrix from a finite
     * difference scheme in the case of ions.
     */

    // Define the necessary fitting parameters
    // assuming constructor initialized all the necessary fields before
    double mu = m1 * m2 / ( m1 + m2);
    double chisq;
    double q = 0.0;
    double dq = (q_max - q_min) / (n_q - 1);



    // Use parameter 2 as this is done here for ions
    double deltam = get_deriv_convergence(2);


    // This is the final matrix whose columns all have to be interpolated
    coeff_mat = gsl_matrix_alloc(n_q, polydeg+1);

    // Allocate the X-Matrix to perform the linear regression
    X_mat = gsl_matrix_alloc(n_s, polydeg+1);

    auto fitting_ws = gsl_multifit_linear_alloc(n_s, polydeg+1);
    c = gsl_vector_alloc(polydeg+1);
    cov = gsl_matrix_alloc(polydeg+1, polydeg+1);
    u_vals = gsl_vector_alloc(n_s);

    std::ofstream chi_sq_file;

    chi_sq_file.open("chi_diagnostic-mass_derivative_ions.csv");

    std::cout << "Creating pair-action derivative lookup table for the mass derivative of the ions!" << std::endl;

    float prog = 0.0;

    for(int i = 0; i < n_q; i++)
    {
        prog = (float ) i / (float) n_q;
        int barWidth = 100;
        int pos = prog * barWidth;

        std::cout << "[";

        for(int bar = 0; bar < barWidth; ++bar)
        {
            if(bar < pos) std::cout << "=";
            else if (bar == pos) std::cout << ">";
            else std::cout << " ";
        }
        std::cout << "]" << int(prog * 100.0) << "% \r" << std::flush;

        q = i*dq + q_min;
        // set up the necessary values for the s grid
        double s = 0.0;
        double s_max = 0.0;
        double wave_lim = lambda_max * lambda0;
        double slim_up = 2 * q;

        if(slim_up < wave_lim)
        {
            s_max = slim_up;
        }
        else
        {
            s_max = wave_lim;
        }
        const double s_min = 0.0;
        double ds = (s_max - s_min) / (n_s);

        auto deriv_m_diag = ( get_uqs_mass(q, s, m1 , m2  + deltam )- get_uqs_mass(q, s, m1, m2) )
                            / deltam;

        if(isnan(deriv_m_diag))
        {
            std::cout << "Error received NaN from the forward differentiation! \n";
            assert(!isnan(deriv_m_diag));
        }

        q_grid[i] = q;

        for(int j = 0; j < n_s; j++)
        {
            s = j * ds + s_min;
            auto deriv_m_od =  ( get_uqs_mass(q, s, m1 , m2 + deltam )- get_uqs_mass(q, s, m1, m2) )
                               / deltam;

            if(isnan(deriv_m_diag))
            {
                std::cout << "Error received NaN from the forward differentiation! \n";
                assert(!isnan(deriv_m_od));
            }

            double du = deriv_m_od - deriv_m_diag;
            gsl_vector_set(u_vals, j, du);

            // Now we iterate over all columns to build up the observation matrix

            // There will be no zeroth order in the fitting procedure
            gsl_matrix_set(X_mat, j, 0, 0.0);

            for(int k = 1; k <= polydeg; k++)
            {
                // otherwise add the entry (s^2)^k
                gsl_matrix_set(X_mat, j, k, pow(pow(s,2),k));
            }


        }

        // Now obtain the fitting coefficients through linear regression
        gsl_multifit_linear(X_mat, u_vals, c, cov, &chisq, fitting_ws);

        // The u(q,0) coefficient has to be set manually.
        gsl_matrix_set(coeff_mat, i, 0, deriv_m_diag);

        chi_sq_file << q <<", " << chisq << "\n";
        for(int n = 1; n <= polydeg; n++)
        {
            gsl_matrix_set(coeff_mat, i, n, gsl_vector_get(c,n));
        }
    }


    std::cout << "\n";

    gsl_multifit_linear_free(fitting_ws);


    // The fitting procedure is now finished and now the interpolation
    for(int p = 0; p <= polydeg; p++)
    {
        auto vals = get_coeff_mat_column(p);
        accel_lookup[p] = gsl_interp_accel_alloc();
        coeff_splines[p] = gsl_spline_alloc(gsl_interp_cspline, n_q);
        auto err_code = gsl_spline_init((coeff_splines[p]), q_grid.data(), vals.data(), n_q);
        if(err_code)
        {
            std::cout << "Error! Interpolation failed with code: " << gsl_strerror(err_code) << std::endl;
            exit(2);
        }
    }

    std::cout << "Fitting and interpolation finished!" << std::endl;
    chi_sq_file.close();
    std::string table_file_name = "ion_pair_action_mass_deriv.csv";
    print_interpolation_table(table_file_name);



}

void mass_deriv::print_interpolation_table(std::string &fname)
{
    std::ofstream table;
    table.open(fname.c_str());

    // set up the necessary values for the q grid
    double q = 0.0;

    double dq = (q_max - q_min) / (n_q - 1);

    for(int i = 0; i < n_q; i++)
    {
        q = i * dq + q_min;

        table << std::fixed << std::setprecision(12) << q <<" ";
        for(int j = 0; j <= polydeg; j++)
        {
            if(j!= polydeg)
            {
                table << std::fixed << std::setprecision(12) << gsl_matrix_get(coeff_mat, i, j) << " ";
            }
            else
            {
                table << std::fixed << std::setprecision(12) << gsl_matrix_get(coeff_mat, i, j);
            }
        }
        table << "\n";
    }
}


void mass_deriv::print_coefficient_interpolation(int c_id, std::string& fname)
{
    std::ofstream coeff_file;
    coeff_file.open(fname.c_str());
    coeff_file << "# q, A_"<<c_id << "\n";

    for(int i = 0; i < n_q; i++)
    {
        coeff_file << gsl_matrix_get(coeff_mat, i, c_id);
    }
}
