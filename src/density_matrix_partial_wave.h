#pragma once

#include <iostream>
#include <vector>
#include "gsl/gsl_sf_coulomb.h"
#include "gsl/gsl_sf_laguerre.h"
#include "gsl/gsl_integration.h"
#include "gsl/gsl_sf_hermite.h"
#include "gsl/gsl_errno.h"
#include "density_mat_qs.hpp"

#define epsabs 1e-14
#define epsrel 1e-12
double rho_l_bound(double r1, double r2, double beta, double Z, double kappa, int l, int n_cutoff)
{
    /**
     * Calculate the bound part of the l-channel for the density matrix
     */

    double res = 0.0;

    if(r1 == 0 && r2 == 0)
        return 0;

    double Z_abs = abs(Z);
    double res_old = 0.0;
    for(int n = l+1; n < n_cutoff; n++)
    {
        double En = - kappa * Z*Z / (4 * n * n);
        res += gsl_sf_hydrogenicR(n, l, Z_abs, r1) * gsl_sf_hydrogenicR(n, l, Z_abs, r2) * exp(- kappa * En);
        std::cout << "res = " << res << ", res_old = " << res_old << ", diff = " << (res - res_old) / res_old << "\n";
        res_old = res;
    }

    return res;
}

double Coulomb_scattering_state(double k, double Z, double r, int l)
{

    gsl_sf_result F_1, Fp_1,G_1, Gp_1;
    gsl_sf_result F_2, Fp_2,G_2, Gp_2;

    double eta = Z / (2. * k);
    double exp_f, exp_g;
    int status = gsl_sf_coulomb_wave_FG_e(eta, k*r, l, 0, &F_1, &Fp_1, &G_1, &Gp_1, &exp_f, &exp_g);

    if(status != 0)
    {
        //std::cout << "Error thrown! Status = " << gsl_strerror(status) << std::endl;
    }

    return sqrt(2.0 / M_PI) * F_1.val / r;
}

double rho_l_scatt(double k, double r1, double r2, double beta, double Z, double kappa, int l)
{

    if(r1 == 0 && r2 == 0 && l == 0)
    {
        return 2 * Z * k / (exp(M_PI  * Z / k) - 1);
    }

    return exp(- beta * kappa * k * k) * Coulomb_scattering_state(k, Z, r1, l) * Coulomb_scattering_state(k, Z, r2, l);
}


struct rho_l_scatt_specs
{
    double r1, r2, beta, Z, kappa;
    int l;
};

double rho_l_scatt_integrand(double k, void* p)
{
    auto * params = (struct rho_l_scatt_specs *) p;
    double r1 = params->r1;
    double r2 = params->r2;
    double beta = params->beta;
    double Z = params->Z;
    double kappa = params->kappa;
    int l = params->l;

    return rho_l_scatt(k, r1, r2, beta, Z, kappa, l);
}


double rho_l(double r1, double r2, double beta, double kappa, double Z, int l, gsl_integration_workspace* ws, int max_int, int bound_cutoff)
{
    if(r1 == 0 || r2 == 0)
    {
        return 0;
    }
    gsl_function I;
    struct rho_l_scatt_specs rho_params = {r1, r2, beta, Z, kappa, l};

    I.function = &rho_l_scatt_integrand;
    I.params =&rho_params;
    double int_res = 0.0;
    double abs_err = 0.0;
    int status = gsl_integration_qagiu(&I, 0.0, epsabs, epsrel, max_int,  ws, &int_res, &abs_err);
    if(status != 0)
    {
        //std::cout << "Error thrown! Status = " << gsl_strerror(status) << std::endl;
    }

    if(Z > 0.0)
    {
        return int_res;
    }

    double bound_res = rho_l_bound(r1, r2, beta, Z, kappa, l, bound_cutoff);

    return (bound_res + int_res);

}


double rho_coulomb_partial_expansion(double r1, double r2, double theta, double beta, double kappa, double Z, double l_cut, int max_int, int bound_cutoff)
{
    gsl_integration_workspace* ws = gsl_integration_workspace_alloc(max_int);

    double rho = 0.0;
    double cos_theta = cos(theta);

    if(r1 == 0.0 && r2 == 0.0)
    {
        return rho_coulomb_origin(beta, Z, kappa);
    }

    double rho_old  = rho;
    for(int l = 0; l <= l_cut; l++)
    {
        double prefactor = (2. * l + 1.) * gsl_sf_hermite_func(l, cos_theta);
        rho += prefactor *  rho_l(r1, r2, beta, kappa, Z, l, ws, max_int, bound_cutoff);
        std::cout << "Rho = " << rho << ", Rho_old = " << rho_old << ", delta = " << abs(rho - rho_old ) << "\n";
        rho_old = rho;

    }

    return rho / (4. * M_PI);
}