#pragma once

#include <vector>

#include "gsl/gsl_sf_coulomb.h"
#include "gsl/gsl_sf_laguerre.h"
#include "gsl/gsl_sf_legendre.h"
#include "gsl/gsl_math.h"
#include "gsl/gsl_sf_result.h"
#include "gsl/gsl_errno.h"
#include "gsl/gsl_integration.h"


/**
 * This Header file contains the same functionaliy as density_mat.hpp, but with the difference
 * that is just uses the parameters as defined in Militzer2009
 *
 * q = 1/2 * (r1  + r2)
 * s = |\mathbf{r1} - \mathbf{r2}|
 *
 * This reduces the two-body density matrix to a 2D scalar field instead of  a 3D one.
 * The step is required in order to find the appropriate fits for the
 */

#define twopisq 19.739208802178716
#define twopi 6.283185307179586
#define fourpii 0.07957747154594767
#define epsabs 1e-16
#define epsrel 1e-8
#define PRT_WARNING false


double cgrand_qs(double wavek, double q, double s, double beta, double kappa, double Z)
{
    double c_intgrand = 0.0;

    if(wavek == 0.0) return 0;

    double x = q + s / 2.0;
    double y = q - s / 2.0;
    double reconst_val_F;
    double reconst_val_Fp;

    auto xk = wavek * x;
    auto yk = wavek * y;
    double eta = Z / (2.0 * wavek);
    double gibbs = std::exp(- beta * kappa * wavek * wavek);

    gsl_sf_result F_1, Fp_1,G_1, Gp_1;
    gsl_sf_result F_2, Fp_2,G_2, Gp_2;


    double exp_F, exp_G;
    if(x == y)
    {
        if (x == 0.0)
        {
            c_intgrand = (Z / twopi) * wavek * gibbs / (std::exp(Z * M_PI/wavek) - 1.0);
            return c_intgrand;
        }

        int status = gsl_sf_coulomb_wave_FG_e(eta, xk, 0, 0, &F_1, &Fp_1, &G_1, &Gp_1, &exp_F, &exp_G);

        if(status != 0 && PRT_WARNING) {
            std::cout << "Coloumb wave-function computation failed with: " << gsl_strerror(status) << std::endl;
            std::cout << "Eta = " << eta << ", xk = " << xk << ", yk =" << yk << ", F = " << F_1.val << ", Fp_2.val="
                      << Fp_1.val << std::endl;
            std::cout << "exp_F =" << exp_F << ", x  = " << x << ", y = " << y << ", k = " << wavek << std::endl;
            if (status == 16)
            {
                if(2 * eta / xk > 1e3)
                {
                    double b = sqrt((xk / (2 * eta)));
                    double alpha = 2*sqrt(2*eta*xk) - M_PI * eta;

                    F_1.val = 0.5 * beta * exp(alpha);
                    Fp_1.val = 0.5 * 1/beta * exp(alpha);
                }
                else
                    {
                    reconst_val_F = F_1.val * (1 + exp_F + exp_F * exp_F / 2 + std::pow(exp_F, 3) / 6 + std::pow(exp_F, 4) / 24);
                    reconst_val_Fp = Fp_1.val * (1 + exp_F + exp_F * exp_F / 2 + std::pow(exp_F, 3) / 6 + std::pow(exp_F, 4) / 24);
                    F_1.val = reconst_val_F;
                    Fp_1.val = reconst_val_Fp;
                    }

            }
        }
        auto fo2 = wavek*wavek * gibbs * (Fp_1.val*Fp_1.val + (1. - Z / (wavek * wavek * x)) * F_1.val * F_1.val) / twopisq;
        return fo2;

    }

    int status = gsl_sf_coulomb_wave_FG_e(eta, xk, 0, 0, &F_1, &Fp_1, &G_1, &Gp_1, &exp_F, &exp_G);

    if(status != 0 && PRT_WARNING)
    {
        std::cout << "Coloumb wave-function computation failed with: " << gsl_strerror(status) << std::endl;
        std::cout << "Eta = " << eta << ", xk = "  << xk  <<", yk ="<< yk << ", F = " << F_1.val << ", Fp_2.val=" << Fp_1.val << std::endl;
        std::cout << "exp_F =" << exp_F << ", x  = " << x << ", y = " << y << ", k = " << wavek <<std::endl;
        if (status == 16)
        {
            if(2 * eta / xk > 1e3)
            {
                double b = sqrt((xk / (2 * eta)));
                double alpha = 2*sqrt(2*eta*xk) - M_PI * eta;

                F_1.val = 0.5 * beta * exp(alpha);
                Fp_1.val = 0.5 * 1/beta * exp(alpha);
            }
            else
            {
                reconst_val_F = F_1.val * (1 + exp_F + exp_F * exp_F / 2 + std::pow(exp_F, 3) / 6 + std::pow(exp_F, 4) / 24);
                reconst_val_Fp = Fp_1.val * (1 + exp_F + exp_F * exp_F / 2 + std::pow(exp_F, 3) / 6 + std::pow(exp_F, 4) / 24);
                F_1.val = reconst_val_F;
                Fp_1.val = reconst_val_Fp;
            }

        }

    }


    if(y < 1e-8)
    {
        F_2.val = 0.0;
        Fp_2.val = std::sqrt(twopi * eta / (std::exp(twopi * eta)-1.0));
    }
    else
    {
        status = gsl_sf_coulomb_wave_FG_e(eta, yk, 0, 0, &F_2, &Fp_2, &G_1, &Gp_2, &exp_F, &exp_G);

        if(status != 0 && PRT_WARNING)
        {
            std::cout << "Coloumb wave-function computation failed with: " << gsl_strerror(status) << std::endl;
            std::cout << "Eta = " << eta << ", xk = "  << xk  <<", yk ="<< yk << ", F = " << F_2.val << ", Fp_2.val=" << Fp_2.val << std::endl;
            std::cout << "exp_F =" << exp_F << ", x  = " << x << ", y = " << y << ", k = " << wavek <<std::endl;
            if (status == 16)
            {
                if(2 * eta / xk > 1e3)
                {
                    double b = sqrt((xk / (2 * eta)));
                    double alpha = 2*sqrt(2*eta*xk) - M_PI * eta;

                    F_1.val = 0.5 * beta * exp(alpha);
                    Fp_1.val = 0.5 * 1/beta * exp(alpha);
                }
                else
                {
                    reconst_val_F = F_1.val * (1 + exp_F + exp_F * exp_F / 2 + std::pow(exp_F, 3) / 6 + std::pow(exp_F, 4) / 24);
                    reconst_val_Fp = Fp_1.val * (1 + exp_F + exp_F * exp_F / 2 + std::pow(exp_F, 3) / 6 + std::pow(exp_F, 4) / 24);
                    F_1.val = reconst_val_F;
                    Fp_1.val = reconst_val_Fp;
                }

            }

        }

    }

    c_intgrand = wavek * gibbs * (F_1.val * Fp_2.val - F_2.val * Fp_1.val) / (twopisq * s);

    return c_intgrand;

}


struct cgrand_specs_qs
{
    double q, s, beta, kappa, Z;
};


double cgrand_integrand_qs(double wavek, void* p)
{
    auto * params = (struct cgrand_specs_qs *) p;

    double q = params->q;
    double s = params->s;
    double kappa = params -> kappa;
    double beta = params -> beta;
    double Z = params -> Z;

    return cgrand_qs(wavek,q, s, beta, kappa, Z);

}


double bound_state_sum_qs(double q, double s, double beta, double kappa, double z, int n_max_sum, double tol_sum)
{

    double bound = 1500.0;
    double oldbound = 1000.0;
    double tail;

    double sum = 0.0;

    double x = q + s/2.0;
    double y = q - s/2.0;
    double zabs = std::abs(z);


    for(int n = 1; n <= n_max_sum; n++)
    {
        double fxp = 1.0;
        double zxn = zabs * x / n;
        double fx = 1. - zxn;

        double fyp = 1.0;
        double zyn = zabs * y / n;
        double fy = 1.-zyn;

        double fxn, fyn;

        gsl_sf_result fx1, fpx1, fy1, fyp1;
        fxp = gsl_sf_laguerre_n(n-1, 0, zxn);
        fx = gsl_sf_laguerre_n(n, 0, zxn);
        fyp = gsl_sf_laguerre_n(n-1, 0, zyn);
        fy = gsl_sf_laguerre_n(n, 0, zyn);

        double prefac = fourpii * std::exp(0.25 * beta * kappa * z*z / (n*n)) * (0.5 *
                                                                                 std::pow(zabs,3) / std::pow(n, 5.0)) * std::exp(- zabs * q / n);

        double term;
        if (x != y)
        {
            term = (std::pow(n,3.0) / zabs) * prefac * (fxp * fy - fx * fyp) / s;
        }
        else
        {
            if (x != 0.0)
            {
                term = (n*n) * prefac * ((n*n)/(zabs * q) * std::pow(fx-fxp,2.0) + fx * fxp);
            }
            else
            {
                term = n*n * prefac;
            }
        }

        sum = sum + term;

        oldbound = bound;
        tail = (n-1) * (n-1) * term / (2.0 * n  - 1.0);

        bound = sum + tail;

        if(std::abs(bound - oldbound) < tol_sum * std::abs(bound))
        {
            if(PRT_WARNING)
                std::cout << "Terminated at step " << n << " with a tolerance of " << std::abs(bound - oldbound) << std::endl;
            return bound;
        }


    }

    return bound;

}

double rho_coulomb_qs(double q, double s, double beta, double kappa, double z, int n_max_int, int n_max_sum, double tol_sum)
{
    gsl_set_error_handler_off();

    double rho = 0.0;

    struct cgrand_specs_qs params = {q, s, beta, kappa, z};

    gsl_function I;

    I.function = &cgrand_integrand_qs;
    I.params = &params;

    gsl_integration_workspace* ws = gsl_integration_workspace_alloc(n_max_int);
    double int_res, int_err;
    int status = gsl_integration_qagiu(&I, 0.0, epsabs,  epsrel, n_max_int, ws, &int_res, &int_err);
    gsl_integration_workspace_free(ws);


    rho = int_res;
    if(status != 0 && PRT_WARNING)
    {
        std::cout << "Integration failed with: " << gsl_strerror(status) << std::endl;
    }


    if(z >= 0.0)
    {
        return  rho;
    }

    double bound = bound_state_sum_qs(q, s, beta, kappa, z, n_max_sum, tol_sum);

    rho = int_res + bound;
    return rho;



}


double rho_coulomb_qs(double q, double s, double beta, double kappa, double z, gsl_integration_workspace* ws, int n_max_int, int n_max_sum, double tol_sum)
{
    gsl_set_error_handler_off();

    double rho = 0.0;

    struct cgrand_specs_qs params = {q, s, beta, kappa, z};

    gsl_function I;

    I.function = &cgrand_integrand_qs;
    I.params = &params;

    double int_res, int_err;
    int status = gsl_integration_qagiu(&I, 0.0, epsabs,  epsrel, n_max_int, ws, &int_res, &int_err);


    rho = int_res;
    if(status != 0)
    {
        std::cout << "Integration failed with: " << gsl_strerror(status) << std::endl;
    }


    if(z >= 0.0)
    {
        return  rho;
    }

    double bound = bound_state_sum_qs(q, s, beta, kappa, z, n_max_sum, tol_sum);

    rho = int_res + bound;
    return rho;


}

double rho_coulomb_origin(double beta, double Z, double kappa)
{
    /**
     * Implements the Coulomb density matrix for the case of rho(0,0; beta) from eq. (32) in Pollock 1988
     */

    // Create a vector with the cumulative coefficients from Pollock1988
    std::vector<double> p_j = {1.772453851, -0.074137740, 0.005834805, -0.000382686, 0.000008738, 0.000002138, -0.000000356};

    double gamma = beta * kappa * Z * Z;

    double sum = 0.0;
    auto fac = 0.0;

    if(Z < 0)
    {
        fac = -1.0;
    }
    else
    {
        fac = 1.0;
    }

    for(int i = 0; i < p_j.size(); i++)
    {
        sum += p_j[i] * std::pow(gamma, (i+1)/2.0)  * std::pow(fac, i+1.0);
    }


    double res = std::pow(std::abs(Z),3.0) * std::pow( 4 * M_PI * gamma, -3.0/2.0) * std::exp(- sum);

    return res;
}


double rho_coulomb_qs_rom(double q, double s, double beta, double kappa, double z, gsl_integration_romberg_workspace* ws, int n_max_int, int n_max_sum, double tol_sum)
{
    gsl_set_error_handler_off();

    double rho = 0.0;

    struct cgrand_specs_qs params = {q, s, beta, kappa, z};

    gsl_function I;

    I.function = &cgrand_integrand_qs;
    I.params = &params;

    double int_res, int_err;
    double b = sqrt(5.0 / beta * kappa);
    size_t n_eval = 0;

    int status = gsl_integration_romberg(&I, 0, 2*b, 1e-8, 1e-12, &int_res, &n_eval, ws);

    rho = int_res;
    if(status != 0)
    {
        std::cout << "Integration failed with: " << gsl_strerror(status) << std::endl;
    }


    if(z >= 0.0)
    {
        return  rho;
    }


    double bound = bound_state_sum_qs(q, s, beta, kappa, z, n_max_sum, tol_sum);
    rho = int_res + bound;


    return rho;

}



double rho_free_qs(double s, double kappa, double beta)
{
    return exp(- s*s / (4 * kappa * beta)) / pow(4*M_PI * kappa * beta, 3./2);
}


double u_free_qs(double s, double kappa, double beta)
{
    return -s*s/(4 * kappa * beta) - 3./2. * log(4 * M_PI * kappa * beta);
}


double u_qs(double rho, double s, double kappa, double beta)
{
    // This function calculates the action, not the two-body pair-potential
    return  u_free_qs(s, kappa, beta) - log(rho);
}


double u_cusp_qs(double q, double s, double beta, double Z, double kappa)
{
    double rho_orig = rho_coulomb_origin(beta, Z, kappa);

    double u_cusp = - log(rho_orig / rho_free_qs(0.0, kappa, beta)) - Z / (2) * 2*q;

    return u_cusp;

}


double calc_u_prim(double r1, double r2, double z, double kappa)
{
    return z * kappa * ( 1./r1 + 1./r2)  * 0.5;
}


double calc_u_tot(double q, double s, double beta, double kappa, double z, int n_max_int, int n_max_sum, double tol_sum)
{
    double rho = rho_coulomb_qs(q, s, beta, kappa, z, 5000, 1500, 1e-12);

    return u_free_qs(s, kappa, beta) - log(rho);

}