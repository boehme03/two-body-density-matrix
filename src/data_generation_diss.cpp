/**
 * Author: Maximilian Böhme
 * Date: 16/02/2023
 * This file is here to calculate the test-data from Pollock and Militzer with different
 */


#include <iostream>
#include <vector>
#include <string>
#include "rapidcsv.h"
#include "gsl/gsl_integration.h"
#include "cfg_file_handler.hpp"
#include "density_mat_qs.hpp"
#include "density_mat_qs_beta_deriv.hpp"

double calc_absolute_val(std::vector<double>& v)
{
    double res = 0.0;
    for(int i = 0; i < 3; i++)
    {
        res += v[i] * v[i];
    }

    return sqrt(res);
}

double calc_diff_val(std::vector<double>& v1, std::vector<double>& v2)
{
    double res = 0.0;

    for(int i = 0; i < 3; i++)
    {
        res += std::pow(v1[i] - v2[i], 2);
    }

    return sqrt(res);
}

void gen_pollock_data()
{
    double Z1 = 1.0;
    double Z2 = 1.0;

    double m1 = 1.0;
    double m2 = 1.0;


    double beta = 1.0;
    int P = 1;

    double l = 2;

    int n_s = 100;
    int n_q = 100;


    double epsilon = beta / P;
    // Initialize the data-structures
    double mu = m1 * m2 / ( m1 + m2);
    double kappa = 1/(2 * mu);
    double z = Z1*Z2 / kappa;
    double lambda = sqrt(4*kappa*epsilon);


    double r_min = 0.0;
    double r_max = 3.0;

    double dr = 0.1;
    double r = r_min;

    // Include the zero step
    int nr = (int) (r_max / dr) + 1;


    std::cout << "Parameters: Z1 = "<< Z1 << ", Z2 = "<< Z2 << ", beta = " << beta << std::endl;
    std::cout << "m1 = "<< m1 << ", m2 = "<< m2 << ", kappa = " << kappa << ", Z = " << z << std::endl;

    std::vector<double> rho_coulomb(nr);

    std::string fname("diss_data_pollock_e-e_diag.csv");

    double r1,r2;
    double q;
    double rho;


    std::ofstream out_file;
    out_file.open("../../plotting/"+fname);


    for(int i = 0; i < nr; ++i)
    {
        r = r_min + i * dr;


        // diagonal calculation
        r1 = r;
        r2 = r;
        q = 0.5 *  (r1  + r2);

        double s = 0;

        std::cout << "q = " << q <<", s = "<< s <<", r1 = " << r1 << ", r2 = " << r2 << std::endl;

        double rho1 = rho_coulomb_qs(q, s, epsilon, kappa, z, 5000, 1200, 1e-12);
        double rhof = rho_free_qs(s, kappa, epsilon);
        double ufree = u_free_qs(s, kappa, epsilon);
        double u = ufree - log(rho1);

        out_file << q << ", " << s << ", " << u / epsilon << ", " << rho1 << ", " << rhof << ", " << ufree << std::endl;

    }

    out_file.close();

    fname = "diss_data_pollock_e-e_off_diag.csv";
    out_file.open("../../plotting/"+fname);

    for(int i = 0; i < nr; ++i)
    {
        r = r_min + i * dr;

        r1 = r;
        r2 = -r;

        std::vector<double> vec1 = {r1, 0, 0};
        std::vector<double> vec2 = {r2, 0, 0};

        q = 0.5 *  (calc_absolute_val(vec1)  + abs(r2));

        double s = calc_diff_val(vec1, vec2);;

        std::cout << "q = " << q <<", s = "<< s <<", r1 = " << r1 << ", r2 = " << r2 << std::endl;

        double rho1 = rho_coulomb_qs(q, s, epsilon, kappa, z, 5000, 1200, 1e-12);
        double rhof = rho_free_qs(s, kappa, epsilon);
        double ufree = u_free_qs(s, kappa, epsilon);
        double u = ufree - log(rho1);

        out_file << q << ", " << s << ", " << u / epsilon << ", " << rho1 << ", " << rhof << ", " << ufree << std::endl;

    }
    out_file.close();
}

void extract_pollock_data_diag_e_e()
{
    double Z1 = 1.0;
    double Z2 = 1.0;

    double m1 = 1.0;
    double m2 = 1.0;


    double beta = 1.0;
    int P = 1;

    double l = 2;

    int n_s = 100;
    int n_q = 100;


    double epsilon = beta / P;
    // Initialize the data-structures
    double mu = m1 * m2 / ( m1 + m2);
    double kappa = 1/(2 * mu);
    double z = Z1*Z2 / kappa;
    double lambda = sqrt(4*kappa*epsilon);


    double r_min = 0.0;
    double r_max = 3.0;

    double dr = 0.1;
    double r = r_min;

    // Include the zero step
    int nr = (int) (r_max / dr) + 1;

    std::cout << "Extracting the pair density matrix for Pollock e-e\n";


    std::cout << "Parameters: Z1 = "<< Z1 << ", Z2 = "<< Z2 << ", beta = " << beta << std::endl;
    std::cout << "m1 = "<< m1 << ", m2 = "<< m2 << ", kappa = " << kappa << ", Z = " << z << std::endl;

    std::vector<double> rho_coulomb(nr);

    std::string fname("rhom_vs_adap_e-e_pollock_diag.csv");

    double r1,r2;
    double q;
    double rho_adap;
    double rho_rhom;
    double u_rhom, u_adap;
    double u_prim;
    double rho_prim;
    std::ofstream out_file;
    out_file.open("../../plotting/"+fname);

    gsl_integration_romberg_workspace* ws = gsl_integration_romberg_alloc(20);
    for(int i = 0; i < nr; i++)
    {
        r = r_min + i * dr;

        r1 = r;
        r2 = r;

        std::vector<double> vec1 = {r1, 0, 0};
        std::vector<double> vec2 = {r2, 0, 0};

        q = 0.5 * (calc_absolute_val(vec1) + abs(r2));

        double s = calc_diff_val(vec1, vec2);

        rho_rhom = rho_coulomb_qs_rom(q, s, beta, kappa, z, ws,  5000, 1200, 1e-12);
        rho_adap = rho_coulomb_qs(q, s, beta, kappa, z, 5000, 1200, 1e-12);

        u_rhom = u_qs(rho_rhom, s, kappa, beta);
        u_adap = u_qs(rho_adap, s, kappa, beta);

        out_file << r << ", " << rho_adap  << ", " << rho_rhom << ", " << u_adap << ", " << u_rhom << "\n";

    }

    out_file.close();


}

void extract_pollock_data_offdiag_e_e()
{
    double Z1 = 1.0;
    double Z2 = 1.0;

    double m1 = 1.0;
    double m2 = 1.0;


    double beta = 1.0;
    int P = 1;

    double l = 2;

    int n_s = 100;
    int n_q = 100;


    double epsilon = beta / P;
    // Initialize the data-structures
    double mu = m1 * m2 / ( m1 + m2);
    double kappa = 1/(2 * mu);
    double z = Z1*Z2 / kappa;
    double lambda = sqrt(4*kappa*epsilon);


    double r_min = 0.0;
    double r_max = 3.0;

    double dr = 0.1;
    double r = r_min;

    // Include the zero step
    int nr = (int) (r_max / dr) + 1;

    std::cout << "Extracting the pair density matrix for Pollock e-e\n";


    std::cout << "Parameters: Z1 = "<< Z1 << ", Z2 = "<< Z2 << ", beta = " << beta << std::endl;
    std::cout << "m1 = "<< m1 << ", m2 = "<< m2 << ", kappa = " << kappa << ", Z = " << z << std::endl;

    std::vector<double> rho_coulomb(nr);

    std::string fname("rhom_vs_adap_e-e_pollock_off_diag.csv");

    double r1,r2;
    double q;
    double rho_adap;
    double rho_rhom;
    double u_rhom, u_adap;
    double u_prim;
    double rho_prim;
    std::ofstream out_file;
    out_file.open("../../plotting/"+fname);

    gsl_integration_romberg_workspace* ws = gsl_integration_romberg_alloc(20);
    for(int i = 0; i < nr; i++)
    {
        r = r_min + i * dr;

        r1 = r;
        r2 = -r;

        std::vector<double> vec1 = {r1, 0, 0};
        std::vector<double> vec2 = {r2, 0, 0};

        q = 0.5 * (calc_absolute_val(vec1) + abs(r2));

        double s = calc_diff_val(vec1, vec2);

        rho_rhom = rho_coulomb_qs_rom(q, s, beta, kappa, z, ws,  5000, 1200, 1e-12);
        rho_adap = rho_coulomb_qs(q, s, beta, kappa, z, 5000, 1200, 1e-12);

        u_rhom = u_qs(rho_rhom, s, kappa, beta);
        u_adap = u_qs(rho_adap, s, kappa, beta);

        out_file << r << ", " << rho_adap  << ", " << rho_rhom << ", " << u_adap << ", " << u_rhom << "\n";

    }

    out_file.close();


}

void extract_pollock_data_offdiag_e_p()
{
    double Z1 = 1.0;
    double Z2 = -1.0;

    double m1 = 1.0;
    double m2 = 1836.0;


    double beta = 10.0;
    int P = 1;

    double l = 2;

    int n_s = 100;
    int n_q = 100;


    double epsilon = beta / P;
    // Initialize the data-structures
    double mu = m1 * m2 / ( m1 + m2);
    double kappa = 1/(2 * mu);
    double z = -2.0;//Z1*Z2 / kappa;
    double lambda = sqrt(4*kappa*epsilon);


    double r_min = 0.0;
    double r_max = 3.0;

    double dr = 0.1;
    double r = r_min;

    // Include the zero step
    int nr = (int) (r_max / dr) + 1;

    std::cout << "Extracting the pair density matrix for Pollock e-e\n";


    std::cout << "Parameters: Z1 = "<< Z1 << ", Z2 = "<< Z2 << ", beta = " << beta << std::endl;
    std::cout << "m1 = "<< m1 << ", m2 = "<< m2 << ", kappa = " << kappa << ", Z = " << z << std::endl;

    std::vector<double> rho_coulomb(nr);

    std::string fname("rhom_vs_adap_e-p_pollock_off_diag.csv");

    double r1,r2;
    double q;
    double rho_adap;
    double rho_rhom;
    double u_rhom, u_adap;
    double u_prim;
    double rho_prim;
    std::ofstream out_file;
    out_file.open("../../plotting/"+fname);

    gsl_integration_romberg_workspace* ws = gsl_integration_romberg_alloc(20);
    for(int i = 0; i < nr; i++)
    {
        r = r_min + i * dr;

        r1 = r;
        r2 = -r;

        std::vector<double> vec1 = {r1, 0, 0};
        std::vector<double> vec2 = {r2, 0, 0};

        q = 0.5 * (calc_absolute_val(vec1) + abs(r2));

        double s = calc_diff_val(vec1, vec2);

        rho_rhom = rho_coulomb_qs_rom(q, s, beta, kappa, z, ws,  5000, 1200, 1e-12);
        rho_adap = rho_coulomb_qs(q, s, beta, kappa, z, 5000, 1200, 1e-12);

        u_rhom = u_qs(rho_rhom, s, kappa, beta);
        u_adap = u_qs(rho_adap, s, kappa, beta);

        out_file << r << ", " << rho_adap  << ", " << rho_rhom << ", " << u_adap << ", " << u_rhom << "\n";

    }

    out_file.close();


}


void extract_pollock_data_diag_e_p()
{
    double Z1 = 1.0;
    double Z2 = -1.0;

    double m1 = 1.0;
    double m2 = 1836.0;


    double beta = 10.0;
    int P = 1;

    double l = 2;

    int n_s = 100;
    int n_q = 100;


    double epsilon = beta / P;
    // Initialize the data-structures
    double mu = m1 * m2 / ( m1 + m2);
    double kappa = 1/(2 * mu);
    double z = -2.0;//Z1*Z2 / kappa;
    double lambda = sqrt(4*kappa*epsilon);


    double r_min = 0.0;
    double r_max = 3.0;

    double dr = 0.1;
    double r = r_min;

    // Include the zero step
    int nr = (int) (r_max / dr) + 1;

    std::cout << "Extracting the pair density matrix for Pollock e-e\n";


    std::cout << "Parameters: Z1 = "<< Z1 << ", Z2 = "<< Z2 << ", beta = " << beta << std::endl;
    std::cout << "m1 = "<< m1 << ", m2 = "<< m2 << ", kappa = " << kappa << ", Z = " << z << std::endl;

    std::vector<double> rho_coulomb(nr);

    std::string fname("rhom_vs_adap_e-p_pollock_diag.csv");

    double r1,r2;
    double q;
    double rho_adap;
    double rho_rhom;
    double u_rhom, u_adap;
    double u_prim;
    double rho_prim;
    std::ofstream out_file;
    out_file.open("../../plotting/"+fname);

    gsl_integration_romberg_workspace* ws = gsl_integration_romberg_alloc(20);
    for(int i = 0; i < nr; i++)
    {
        r = r_min + i * dr;

        r1 = r;
        r2 = r;

        std::vector<double> vec1 = {r1, 0, 0};
        std::vector<double> vec2 = {r2, 0, 0};

        q = 0.5 * (calc_absolute_val(vec1) + abs(r2));

        double s = calc_diff_val(vec1, vec2);

        rho_rhom = rho_coulomb_qs_rom(q, s, beta, kappa, z, ws,  5000, 1200, 1e-12);
        rho_adap = rho_coulomb_qs(q, s, beta, kappa, z, 5000, 1200, 1e-12);

        u_rhom = u_qs(rho_rhom, s, kappa, beta);
        u_adap = u_qs(rho_adap, s, kappa, beta);

        out_file << r << ", " << rho_adap  << ", " << rho_rhom << ", " << u_adap << ", " << u_rhom << "\n";

    }

    out_file.close();


}


void get_data_comparison_militzer_off_diag()
{

}


int main()
{
    extract_pollock_data_diag_e_e();
    extract_pollock_data_offdiag_e_e();
    extract_pollock_data_diag_e_p();
    extract_pollock_data_offdiag_e_p();
}