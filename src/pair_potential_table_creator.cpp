#include <iostream>
#include <fstream>
#include <string>

#include <boost/program_options.hpp>

#include "pair_action_lookup_table.hpp"


namespace po = boost::program_options;
using namespace std;

int main(int arg_c, char* argv[])

{

    /**
     * This tool creates the lookup table of the pair potential.
     */

    double beta, z1, z2, m1, m2, l, theta;
    double dq;
    int P, polydeg, n_s, n_q, n_lambda;
    int N;
    double rs;

    std::string config_file;

    po::options_description generic("Generic options");
    generic.add_options()
            ("version,v", "print version string")
            ("help", "produce help message")
            ("config,c", po::value<std::string>(&config_file)->default_value("table.cfg"),
             "name of a file of a configuration.")
            ;



    po::options_description config("Configuration");
    config.add_options()
            ("theta", po::value<double>(&theta)->default_value(1.0), "inverse temperature in the simulation in units of the fermi energy")
            ("z_1", po::value<double>(&z1)->default_value(1.0), "Charge of the first particle species")
            ("z_2", po::value<double>(&z2)->default_value(1.0), "Charge of the first particle species")
            ("m_1", po::value<double>(&m1)->default_value(1.0), "Mass of the first particle species in electron masses")
            ("m_2", po::value<double>(&m2)->default_value(1.0), "Mass of the second particle species in electron masses")
            ("P", po::value<int>(&P)->default_value(1.0), "Numbe of imaginary time-slices")
            ("polydeg", po::value<int>(&polydeg)->default_value(1.0), "Degree of the fitting polynomial")
            ("n_s", po::value<int>(&n_s)->default_value(1.0), "Number of points to be sampled in s-space")
            ("dq", po::value<double>(&dq)->default_value(1.0), "Number of points to be sampled in q-space")
            ("n_lambda", po::value<int>(&n_lambda)->default_value(2), "Number of thermal wavelengths to consider before using the primitive approximation")
            ("rs", po::value<double>(&rs)->default_value(1.0), "Brueckner parameter of the simulation")
            ("N", po::value<int>(&N)->default_value(1), "Number of particles in the simulation box")
            ;

    po::options_description cmdline_options;
    cmdline_options.add(generic).add(config);

    po::options_description config_file_options;
    config_file_options.add(config);

    po::options_description visible("Allowed options");
    visible.add(generic).add(config);

    po::positional_options_description p;
    p.add("input-file", -1);

    po::variables_map vm;
    store(po::command_line_parser(arg_c, argv).options(cmdline_options).positional(p).run(), vm);
    notify(vm);



    std::ifstream ifs(config_file.c_str());
    if (!ifs)
    {
        std::cout << "can not open config file: " << config_file << "\n";
        return 0;
    }
    else
    {
        store(parse_config_file(ifs, config_file_options), vm);
        notify(vm);
    }

    if (vm.count("help"))
    {
        std::cout << visible << "\n";
        return 0;
    }

    l = std::pow(4. * M_PI * N / 3.0, 1.0 / 3.0) * rs;
    double mu = (m1 * m2) / (m1 + m2);
    double kappa = 1/(2 * mu);
    double z = z1 * z2 * mu;

    // This is for the Spin-Unpolarized case
    beta = 2.0 * l * l * pow(6.0 * N / 2.0 * M_PI * M_PI, -2.0/3.0) / theta;
    double epsilon = beta / double(P);

    double lambda_th = sqrt(4 * M_PI * kappa * epsilon);

    double q_max = sqrt(3.0) / 2. * l;
    double q_min = 0.0;

    n_q = ceil(((q_max - q_min) / (dq * l ) + 1 )  );


    cout << "Calculating pair action derivative lookup table for the following parameter sets \n";
    cout << "|------------------------------------------------------------------------|\n";
    cout << "z1 = " << z1 << ", z2 = " << z2 << ", m1 = " << m1 << " , m2 = " <<  m2 << "\n";
    cout << "P = " << P << ", polydeg = " << polydeg << ", n_s = " << n_s << " , dq = " <<  dq << ", l = " << l << "\n";
    cout << "n_lambda = " << n_lambda <<", rs = " << rs << ", N_part = " <<  N << ", theta = " << theta << ",  beta = " << beta << " \n";
    cout << "|------------------------------------------------------------------------|\n";



    pair_action_lookup_table pair_pot_table_creator(z1, z2, m1, m2, l, beta, n_s, n_q, polydeg, P, false, 5000, 1500, 1e-12, n_lambda);


    return 0;
}