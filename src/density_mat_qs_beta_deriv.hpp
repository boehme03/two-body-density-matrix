#pragma once
#include "gsl/gsl_sf_coulomb.h"
#include "gsl/gsl_sf_laguerre.h"
#include "gsl/gsl_sf_legendre.h"
#include "gsl/gsl_math.h"
#include "gsl/gsl_sf_result.h"
#include "gsl/gsl_errno.h"
#include "gsl/gsl_integration.h"

#include "density_mat_qs.hpp"

/**
 * This Header file contains the beta derivative of the two-body density matrix as described in Militzer 2019 and Pollock 1988
 *
 */


double cgrand_qs_deriv(double wavek, double q, double s, double beta, double kappa, double Z, double P)
{
    double c_intgrand = 0.0;

    if(wavek == 0.0) return 0;

    double x = q + s / 2.0;
    double y = q - s / 2.0;


    auto xk = wavek * x;
    auto yk = wavek * y;
    double eta = Z / (2.0 * wavek);

    // The Gibbs factor is the only term that is actually modified
    double gibbs = std::exp(- beta/P * kappa * wavek * wavek) * (- kappa * wavek * wavek / P);

    gsl_sf_result F_1, Fp_1,G_1, Gp_1;
    gsl_sf_result F_2, Fp_2,G_2, Gp_2;


    double exp_F, exp_G;
    if(x == y)
    {
        if (x == 0.0)
        {
            c_intgrand = (Z / twopi) * wavek * gibbs * Z * Z / (std::exp(Z * M_PI/wavek) - 1.0);
            return c_intgrand;
        }

        int status = gsl_sf_coulomb_wave_FG_e(eta, xk, 0, 0, &F_1, &Fp_1, &G_1, &Gp_1, &exp_F, &exp_G);

        if(status != 0 && PRT_WARNING)
        {
            std::cout << "Coloumb wave-function computation failed with: " << gsl_strerror(status) << std::endl;
            std::cout << "Eta = " << eta << ", yk = "  << xk << ", F = " << F_1.val << ", Fp_2.val=" << Fp_1.val << std::endl;
            std::cout << "exp_F =" << exp_F << ", x  = " << x << ", y = " << y << ", k = " << wavek <<std::endl;
        }

        auto fo2 = wavek * wavek * gibbs * (Fp_1.val*Fp_1.val + (1. - Z / (wavek * wavek * x)) * F_1.val * F_1.val) / twopisq;
        return fo2;

    }

    int status = gsl_sf_coulomb_wave_FG_e(eta, xk, 0, 0, &F_1, &Fp_1, &G_1, &Gp_1, &exp_F, &exp_G);

    if(status != 0 && PRT_WARNING)
    {
        std::cout << "Coloumb wave-function computation failed with: " << gsl_strerror(status) << std::endl;
        std::cout << "Eta = " << eta << ", yk = "  << xk << ", F = " << F_1.val << ", Fp_2.val=" << Fp_1.val << std::endl;
        std::cout << "exp_F =" << exp_F << ", x  = " << x << ", y = " << y << ", k = " << wavek <<std::endl;
    }


    if(y < 1e-8)
    {
        F_2.val = 0.0;
        Fp_2.val = std::sqrt(twopi * eta / (std::exp(twopi * eta)-1.0));
    }
    else
    {
        status = gsl_sf_coulomb_wave_FG_e(eta, yk, 0, 0, &F_2, &Fp_2, &G_1, &Gp_2, &exp_F, &exp_G);

        if(status != 0 && PRT_WARNING)
        {
            std::cout << "Coloumb wave-function computation failed with: " << gsl_strerror(status) << std::endl;
            std::cout << "Eta = " << eta << ", yk = "  << yk << ", F = " << F_2.val << ", Fp_2.val=" << Fp_2.val << std::endl;
            std::cout << "exp_F =" << exp_F << ", x  = " << x << ", y = " << y << ", k = " << wavek <<std::endl;
        }

    }

    c_intgrand = wavek * gibbs * (F_1.val * Fp_2.val - F_2.val * Fp_1.val) / (twopisq * s);

    return c_intgrand;

}


struct cgrand_specs_qs_deriv
{
    double q, s, beta, kappa, Z, P;
};


double cgrand_integrand_qs_deriv(double wavek, void* p)
{
    auto * params = (struct cgrand_specs_qs_deriv *) p;

    double q = params->q;
    double s = params->s;
    double kappa = params -> kappa;
    double beta = params -> beta;
    double Z = params -> Z;
    double P = params->P;

    return cgrand_qs_deriv(wavek,q, s, beta, kappa, Z, P);

}


double bound_state_sum_qs_deriv(double q, double s, double beta, double kappa, double z, double P, int n_max_sum, double tol_sum)
{

    double bound = 1500.0;
    double oldbound = 1000.0;
    double tail;

    double sum = 0.0;

    double x = q + s/2.0;
    double y = q - s/2.0;
    double zabs = std::abs(z);


    for(int n = 1; n <= n_max_sum; n++)
    {
        double fxp = 1.0;
        double zxn = zabs * x / n;
        double fx = 1. - zxn;

        double fyp = 1.0;
        double zyn = zabs * y / n;
        double fy = 1.-zyn;

        double fxn, fyn;

        gsl_sf_result fx1, fpx1, fy1, fyp1;
        fxp = gsl_sf_laguerre_n(n-1, 0, zxn);
        fx = gsl_sf_laguerre_n(n, 0, zxn);
        fyp = gsl_sf_laguerre_n(n-1, 0, zyn);
        fy = gsl_sf_laguerre_n(n, 0, zyn);

        double prefac = (0.25 / P * kappa * z*z / (n*n)) * fourpii * std::exp(0.25 * beta/P * kappa * z*z / (n*n)) * (0.5 *
                                                                                 std::pow(zabs,3) / std::pow(n, 5.0)) * std::exp(- zabs * q / n);

        double term;
        if (x != y)
        {
            term = (std::pow(n,3.0) / zabs) * prefac * (fxp * fy - fx * fyp) / s;
        }
        else
        {
            if (x != 0.0)
            {
                term = (n*n) * prefac * ((n*n)/(zabs * q) * std::pow(fx-fxp,2.0) + fx * fxp);
            }
            else
            {
                term = n*n * prefac;
            }
        }

        sum = sum + term;

        oldbound = bound;
        tail = (n-1) * (n-1) * term / (2.0 * n  - 1.0);

        bound = sum + tail;

        if(std::abs(bound - oldbound) < tol_sum * std::abs(bound))
        {
            //std::cout << "Terminated at step " << n << " with a tolerance of " << std::abs(bound - oldbound) << std::endl;
            return bound;
        }


    }

    return bound;

}


double rho_coulomb_qs_deriv(double q, double s, double beta, double kappa, double z, double P, int n_max_int, int n_max_sum, double tol_sum)
{
    gsl_set_error_handler_off();

    double rho = 0.0;

    struct cgrand_specs_qs_deriv params = {q, s, beta, kappa, z, P};

    gsl_function I;

    I.function = &cgrand_integrand_qs_deriv;
    I.params = &params;

    gsl_integration_workspace* ws = gsl_integration_workspace_alloc(n_max_int);
    double int_res, int_err;
    int status = gsl_integration_qagiu(&I, 0.0, epsabs,  epsrel, n_max_int, ws, &int_res, &int_err);
    gsl_integration_workspace_free(ws);


    rho = int_res;
    if(status != 0 && PRT_WARNING)
    {
        std::cout << "Integration failed with: " << gsl_strerror(status) << std::endl;
    }


    if(z >= 0.0)
    {
        return  rho;
    }

    double bound = bound_state_sum_qs_deriv(q, s, beta, kappa, z, P, n_max_sum, tol_sum);

    rho = int_res + bound;
    return rho;



}


double rho_coulomb_qs_deriv(double q, double s, double beta, double kappa, double z, double P, gsl_integration_workspace* ws, int n_max_int, int n_max_sum, double tol_sum)
{
    gsl_set_error_handler_off();

    double rho = 0.0;

    struct cgrand_specs_qs_deriv params = {q, s, beta, kappa, z, P};

    gsl_function I;

    I.function = &cgrand_integrand_qs_deriv;
    I.params = &params;

    double int_res, int_err;
    int status = gsl_integration_qagiu(&I, 0.0, epsabs,  epsrel, n_max_int, ws, &int_res, &int_err);


    rho = int_res;
    if(status != 0 && PRT_WARNING)
    {
        std::cout << "Integration failed with: " << gsl_strerror(status) << std::endl;
    }


    if(z >= 0.0)
    {
        return  rho;
    }

    double bound = bound_state_sum_qs_deriv(q, s, beta, kappa, z, P, n_max_sum, tol_sum);

    rho = int_res + bound;
    return rho;

}

// This function implements the origin value expansion of the HTDM in the attractive case
double rho_beta_deriv_origin_attractive(double beta, double P, double kappa, double z)
{

    double epsilon = beta / P;
    // Create a vector with the cumulative coefficients from Pollock1988
    const std::vector<double> p_j = {1.772453851, -0.074137740, 0.005834805, -0.000382686, 0.000008738, 0.000002138, -0.000000356};

    double rho_origin = rho_coulomb_origin(epsilon, z, kappa);

    double sum1 = -3./2. * (1./beta) * rho_origin;

    double sum2 = 0.0;

    double gamma_eps = (epsilon* kappa * z*z);

    for(size_t i = 0; i < p_j.size(); i++)
    {
        double j = i+1;
        sum2 += - p_j[i] * std::pow(gamma_eps, j/2.) * std::pow(-1.0, j) * (j/2.) * 1/beta;
    }

    sum2 *= rho_origin;


    return sum1 + sum2;

}


// This function implements the origin value expansion of the HTDM in the attractive case
double rho_beta_deriv_origin_repulsive(double beta, double P, double kappa, double z)
{

    double epsilon = beta / P;
    // Create a vector with the cumulative coefficients from Pollock1988
    const std::vector<double> p_j = {1.772453851, -0.074137740, 0.005834805, -0.000382686, 0.000008738, 0.000002138, -0.000000356};

    double rho_origin = rho_coulomb_origin(epsilon, z, kappa);

    double sum1 = -3./2. * (1./beta) * rho_origin;

    double sum2 = 0.0;

    double gamma_eps = (epsilon* kappa * z*z);

    for(size_t i = 0; i < p_j.size(); i++)
    {
        double j = i+1;
        sum2 +=  - p_j[i] * std::pow(gamma_eps, j/2.) * (j/2.) * 1/beta;
    }

    sum2 *= rho_origin;


    return sum1  + sum2;

}



double u_beta_deriv_prim_approx(double r1, double r2, double P, double kappa, double z)
{
    return z/P * kappa * ( 1./r1 + 1./r2)  * 0.5;
}

double u_beta_deriv(double q, double s, double beta, double P, double kappa, double z)
{

    auto epsilon = beta / P;
    double rho_c = 0.0;

    if(q == 0.0)
    {


        rho_c = rho_coulomb_origin(epsilon, z, kappa);
        if(z < 0.0)
        {

            double sum1 = (1 / epsilon * (-3. / 2. + (s * s) / (4. * kappa * epsilon)) -
                          1 / rho_c * rho_beta_deriv_origin_attractive(beta, 1.0, kappa, z) ) * 1/P ;
            return sum1;
        }
        else
        {
            double sum1 = (1 / epsilon * (-3. / 2. + (s * s)  / (4 * kappa * epsilon)) -
                          1 / rho_c * rho_beta_deriv_origin_repulsive(epsilon, 1.0, kappa, z) )* 1/ P ;
            return sum1;
        }
    }

    rho_c = rho_coulomb_qs(q, s, epsilon, kappa, z, 5000, 2000, 1e-12);
    double rho_deriv = rho_coulomb_qs_deriv(q, s, beta, kappa, z, P, 5000, 1500, 1e-12);

    double sum1 = 1/beta * (-3. / 2. +(s*s)*P/(4*kappa*beta));
    double sum2 = - 1/rho_c * rho_deriv;
    return sum1 + sum2;
}