#define CATCH_CONFIG_MAIN
#include <vector>
#include <catch2/catch.hpp>
#include <iostream>
#include <fstream>
#include "../density_mat_qs.hpp"
#include "../rapidcsv.h"


TEST_CASE("Rho_coulomb origin values e-e", "[rho_coulomb_qs]")
{

    double kappa = 1.0;
    double z = 1.0;
    double beta = 0.125;
    double q = 0.0;

    double u_origin_pollock = u_free_qs(q, kappa, beta) - log(rho_coulomb_qs(q, 0.0, beta, kappa, z, 5000, 1200, 1e-12));
    double u_origin_series = u_free_qs(q, kappa, beta) -  log(rho_coulomb_origin(beta, z, kappa));

    CHECK(u_origin_pollock == Approx(u_origin_series).epsilon(1e-5));



}


TEST_CASE("Rho_coulomb origin values e-p", "[rho_coulomb_qs]")
{

    double kappa = 0.5;
    double z = -2.0;
    double beta = 0.125;
    double q = 0.0;

    double u_origin_pollock = u_free_qs(q, kappa, beta) - log(rho_coulomb_qs(q, 0.0, beta, kappa, z, 5000, 1200, 1e-12));
    double u_origin_series = u_free_qs(q, kappa, beta) -  log(rho_coulomb_origin(beta, z, kappa));
    INFO("Pollocks value in the implementation: " << u_origin_pollock);
    INFO("Value in the series implementation: " << u_origin_series);
    CHECK(u_origin_pollock == Approx(u_origin_series).epsilon(1e-5));

    // This only works with a precision of 1e-3
    beta = 10;
    u_origin_pollock = u_free_qs(q, kappa, beta) - log(rho_coulomb_qs(q, 0.0, beta, kappa, z, 5000, 1200, 1e-12));
    u_origin_series = u_free_qs(q, kappa, beta) -  log(rho_coulomb_origin(beta, z, kappa));
    INFO("Pollocks value in the implementation: " << u_origin_pollock);
    INFO("Value in the series implementation: " << u_origin_series);
    CHECK(u_origin_pollock == Approx(u_origin_series).epsilon(1e-3));

    beta = 1;
    u_origin_pollock = u_free_qs(q, kappa, beta) - log(rho_coulomb_qs(q, 0.0, beta, kappa, z, 5000, 1200, 1e-12));
    u_origin_series = u_free_qs(q, kappa, beta) -  log(rho_coulomb_origin(beta, z, kappa));
    INFO("Pollocks value in the implementation: " << u_origin_pollock);
    INFO("Value in the series implementation: " << u_origin_series);
    CHECK(u_origin_pollock == Approx(u_origin_series).epsilon(1e-5));

}