#define CATCH_CONFIG_MAIN
#include <vector>
#include <catch2/catch.hpp>
#include <iostream>
#include <fstream>
#include "../density_mat_qs_beta_deriv.hpp"
#include "../rapidcsv.h"


TEST_CASE("Test beta derivative of potential action e-e case", "")
{
    rapidcsv::Document data_militzer("../test/e-e-data_Militzer.csv");
    std::vector<std::string> columnNames = data_militzer.GetColumnNames();
    UNSCOPED_INFO("Number of columns = " << columnNames.size());

    CHECK(columnNames.size() == 5);


    std::vector<double> uqd = data_militzer.GetColumn<double>("udr0");
    std::vector<double> ud = data_militzer.GetColumn<double>("ur0");
    double kappa = 1.0;
    double z = 1.0;
    double beta = 1.0 / 8.;
    double q = 0.0;
    double dq = 0.1;
    double s = 0.0;
    double P = 1.0;
    int r_idx = 0;

    std::ofstream out_file;

    out_file.open("test_data_comparison_e-e.csv");

    auto uderiv = u_beta_deriv(q, s, beta, P, kappa, z);



    for(int i = 0; i <= 30; i++)
    {
        uderiv = u_beta_deriv(i * dq, s, beta, P, kappa, z);

        out_file << i*dq << ", " << uderiv << ", " << uqd[i] << ", " << std::abs((uderiv - uqd[i])/uqd[i]) << std::endl;

        INFO( "uqd["<< i <<"] = " <<uqd[i] );
        INFO( "uderiv = " << uderiv );
        CHECK(abs((uderiv - uqd[i])/uqd[i]) < 1e-3);

    }

}



TEST_CASE("Test beta derivative of potential action e-p case", "")
{
    rapidcsv::Document data_militzer("../test/e-p-data_Militzer.csv");
    std::vector<std::string> columnNames = data_militzer.GetColumnNames();


    std::vector<double> uqd = data_militzer.GetColumn<double>("uq0d");
    std::vector<double> ud = data_militzer.GetColumn<double>("uq0");
    double kappa = 0.5;
    double z = -2.0;
    double beta = 1.0 / 8.;
    double q = 0.0;
    double dq = 0.1;
    double s = 0.0;
    double P = 1.0;
    int r_idx = 0;

    std::ofstream out_file;

    out_file.open("test_data_comparison_e-p.csv");

    auto uderiv = u_beta_deriv(q, s, beta, P, kappa, z);



    for(int i = 0; i <= 30; i++)
    {
        uderiv = u_beta_deriv(i * dq, s, beta, P, kappa, z);
        out_file << i*dq << ", " << uderiv << ", " << uqd[i] << ", " << std::abs((uderiv - uqd[i])/uqd[i]) << std::endl;

        INFO( "uqd["<< i <<"] = " <<uqd[i] );
        INFO( "uderiv = " << uderiv );
        CHECK(abs(uderiv - uqd[i])/abs(uqd[i]) < 1e-3);

    }

}


TEST_CASE("Test of the off-diagnonal expansion in the e-e case", "")
{
    rapidcsv::Document data_militzer("../test/e-e-data_Militzer.csv");
    std::vector<std::string> columnNames = data_militzer.GetColumnNames();

    // Get the diagonal element
    std::vector<double> uqd = data_militzer.GetColumn<double>("udr0");

    // And the expansion coefficient
    std::vector<double> Adq = data_militzer.GetColumn<double>("Adr");


    double kappa = 1.0;
    double z = 1.0;
    double beta = 1.0 / 8.;
    double q = 1.0;
    int q_idx = int(q*10);
    double dq = 0.1;
    double s = 0.0;
    double P = 1.0;

    double delta_beta = 1e-6;

    double lamb = 4*kappa*beta;
    double ds = 0.0;
    double n_s = 30;
    if(lamb < 2 * q)
    {
        ds = lamb / (n_s - 1);
    }
    if(lamb > 2 * q)
    {
        ds = 2*q / (n_s - 1);
    }

    INFO("A("<<q<<")="<<Adq[q_idx]);
    std::ofstream out_file;
    out_file.open("test_data_off_diagonal_fit_e-e.csv");

    double u_sim;
    double u_interp;
    out_file << "# r, u_sim, u_interp (e-e case) \n";

    double u_1, u_2;
    double u_deriv_num;



    for(int i = 0; i < n_s; i++)
    {

        s = i * ds;

        u_1 = calc_u_tot(q, s, beta, kappa, z, 5000, 1500, 1e-12);
        u_2 = calc_u_tot(q, s, beta+delta_beta, kappa, z, 5000, 1500, 1e-12);

        u_deriv_num = (u_2 - u_1) / delta_beta;

        u_sim = u_beta_deriv(q, s, beta, P, kappa, z);
        u_interp = uqd[q_idx] + Adq[q_idx]*s*s;
        out_file << s << ", " << u_sim << ", " << u_interp <<", " << u_deriv_num << std::endl;
        CHECK(abs(u_sim - u_interp) < 1e-3);
        CHECK(abs(u_deriv_num - u_interp) < 1e-3);

    }
    out_file.close();

 }


TEST_CASE("Test of the off-diagnonal expansion in the e-p case", "")
{
    rapidcsv::Document data_militzer("../test/e-p-data_Militzer.csv");
    std::vector<std::string> columnNames = data_militzer.GetColumnNames();

    // Get the diagonal element
    std::vector<double> uqd = data_militzer.GetColumn<double>("uq0d");

    // And the expansion coefficient
    std::vector<double> Adq = data_militzer.GetColumn<double>("Aqd");


    double kappa = 0.5;
    double z = -2.0;
    double beta = 1.0 / 8.;
    double delta_beta = 1e-6;
    double q = 0.3;
    int q_idx = int(q*10);
    double dq = 0.1;
    double s = 0.0;
    double P = 1.0;


    double lamb = 4*kappa*beta;
    double ds = 0.0;
    double n_s = 30;
    if(lamb < 2 * q)
    {
        ds = lamb / (n_s - 1);
    }
    if(lamb > 2 * q)
    {
        ds = 2*q / (n_s - 1);
    }
    INFO("A("<<q<<")="<<Adq[q_idx]);
    std::ofstream out_file;
    out_file.open("test_data_off_diagonal_fit_e-p.csv");

    double u_sim;
    double u_interp;
    out_file << "# r, u_sim, u_interp (e-p case) \n";


    double u_1, u_2;
    double u_deriv_num;


    for(int i = 0; i < n_s; i++)
    {
        s = i * ds;

        u_1 = calc_u_tot(q, s, beta, kappa, z, 5000, 1500, 1e-12);
        u_2 = calc_u_tot(q, s, beta+delta_beta, kappa, z, 5000, 1500, 1e-12);

        u_deriv_num = (u_2 - u_1) / delta_beta;

        u_sim = u_beta_deriv(q, s, beta, P, kappa, z);
        u_interp = uqd[q_idx] + Adq[q_idx]*s*s;
        out_file << s << ", " << u_sim << ", " << u_interp << ", " << u_deriv_num << std::endl;
        CHECK(abs(u_sim - u_interp)/abs(u_interp) < 1e-3);
        CHECK(abs(u_deriv_num - u_interp)/abs(u_interp) < 1e-3);
    }
    out_file.close();

}

TEST_CASE("Numerical differentiation of the density matrix to beta using forward differences e-e case", "")
{
    double kappa = 1.0;
    double z = 1.0;
    double beta = 0.125;
    double delta_beta = 0.1;
    double q = 2.5;
    double s = 0.0;

    double delta_rho_0 = 1000;
    double rho_deriv = 0.0;


    rapidcsv::Document data_militzer("../test/e-e-data_Militzer.csv");
    std::vector<std::string> columnNames = data_militzer.GetColumnNames();

    std::ofstream conver_file;

    conver_file.open("deriv_conv_forward_e-e_diag.csv");
    conver_file << "# delta_beta deriv_num deriv_an \n";
    // Get the diagonal element
    std::vector<double> uqd = data_militzer.GetColumn<double>("udr0");

    double uqd_burkhard = uqd[1];

    double rho_deriv_an = rho_coulomb_qs_deriv(q, s, beta, kappa, z, 1.0, 5000, 1200, 1e-12);

    double rho_deriv_num = 0.0;

    while(delta_rho_0 > 1e-4)
    {

        auto rho_t1 = rho_coulomb_qs(q, s, beta, kappa, z, 5000, 1200, 1e-12);
        auto rho_t2 = rho_coulomb_qs(q, s, beta + delta_beta, kappa, z, 5000, 1200, 1e-12);
        rho_deriv_num = (rho_t2 - rho_t1) / delta_beta;

        delta_rho_0 = abs(rho_deriv_num - rho_deriv_an);
        delta_beta /= 10;

        if(delta_beta <= 1e-15)
        {
            CHECK(delta_rho_0 < 1e-3);
        }

        conver_file <<  delta_beta <<", "<< rho_deriv_num <<", " << rho_deriv_an << ", " << delta_rho_0 <<"\n";

    }
    std::cout << "rho_deriv_num = " << rho_deriv_num << ", rho_deriv_an =" << rho_deriv_an << ", dRho = " << delta_rho_0 << std::endl;
    std::cout << "delta_beta = " << delta_beta << std::endl;
    INFO("Delta_beta =" << delta_beta );

    CHECK(delta_rho_0 < 1e-3);
}


TEST_CASE("Numerical differentiation of the density matrix to beta using forward differences e-p case", "")
{
    double kappa = 0.5;
    double z = -2.0;
    double beta = 0.125;
    double delta_beta = 0.1;
    double q = 0.3;
    double s = 0.2;

    double delta_rho_0 = 1000;
    double rho_deriv = 0.0;

    rapidcsv::Document data_militzer("../test/e-p-data_Militzer.csv");
    std::vector<std::string> columnNames = data_militzer.GetColumnNames();

    // Get the diagonal element
    std::vector<double> uqd = data_militzer.GetColumn<double>("uq0d");
    std::ofstream conver_file;

    conver_file.open("deriv_conv_forward_e-p.csv");
    conver_file << "# delta_beta deriv_num deriv_an \n";
    // Get the diagonal element

    double uqd_burkhard = uqd[1];

    double rho_deriv_an = rho_coulomb_qs_deriv(q, s, beta, kappa, z, 1.0, 5000, 1200, 1e-12);

    double rho_deriv_num = 0.0;

    while(delta_rho_0 > 1e-4)
    {

        auto rho_t1 = rho_coulomb_qs(q, s, beta, kappa, z, 5000, 1200, 1e-12);
        auto rho_t2 = rho_coulomb_qs(q, s, beta + delta_beta, kappa, z, 5000, 1200, 1e-12);
        rho_deriv_num = (rho_t2 - rho_t1) / delta_beta;

        delta_rho_0 = abs(rho_deriv_num - rho_deriv_an) ;
        delta_beta /= 10;

        if(delta_beta <= 1e-15)
        {
            REQUIRE(delta_rho_0 < 1e-3);
            delta_rho_0=0.0;
        }

        conver_file <<  delta_beta <<", "<< rho_deriv_num <<", " << rho_deriv_an << ", " << delta_rho_0 <<"\n";

    }
    std::cout << "rho_deriv_num = " << rho_deriv_num << ", rho_deriv_an =" << rho_deriv_an << ", dRho = " << delta_rho_0 << std::endl;
    std::cout << "delta_beta = " << delta_beta << std::endl;
    INFO("Delta_beta =" << delta_beta );

    //CHECK(delta_rho_0 < 1e-3);
}

TEST_CASE("Test the action derivative in the e-e case", "")
{
    double kappa = 1.0;
    double z = 1.0;
    double beta = 0.125;
    double delta_beta = 0.1;
    double q = 0.6;
    int q_idx = (int) (q * 10);
    double s = 0.0;

    double delta_rho_0 = 1000;
    double rho_deriv = 0.0;


    rapidcsv::Document data_militzer("../test/e-e-data_Militzer.csv");
    std::vector<std::string> columnNames = data_militzer.GetColumnNames();

    std::ofstream conver_file;

    conver_file.open("action_deriv_conv_forward_e-e_diag.csv");
    conver_file << "# delta_beta deriv_num deriv_an \n";

    // Get the diagonal element
    std::vector<double> uqd = data_militzer.GetColumn<double>("udr0");

    double uqd_burkhard = uqd[q_idx];


    double P = 1;
    double u_deriv_an = u_beta_deriv(q, s, beta, P, kappa, z);

    double u_deriv_num = 0.0;

    while(delta_rho_0 > 1e-4)
    {

        auto u_t1 = calc_u_tot(q, s, beta , kappa, z, 5000, 1200, 1e-12);
        auto u_t2 = calc_u_tot(q, s, beta  + delta_beta, kappa, z, 5000, 1200, 1e-12);
        u_deriv_num = (u_t2 - u_t1) / delta_beta;

        delta_rho_0 = abs(u_deriv_num - u_deriv_an);
        delta_beta /= 10;

        if(delta_beta <= 1e-15)
        {
            CHECK(delta_rho_0 < 1e-3);
            break;

        }

        conver_file <<  delta_beta <<", "<< u_deriv_num <<", " << u_deriv_an << ", " << delta_rho_0 <<"\n";

    }

    std::cout << "u_deriv_num = " << u_deriv_num << ", u_deriv_an =" << u_deriv_an << ", dRho = " << delta_rho_0 << std::endl;
    std::cout << "u_deriv_burkhard = " << uqd_burkhard << std::endl;
    std::cout << "delta_beta = " << delta_beta << std::endl;
    INFO("Delta_beta =" << delta_beta );

    CHECK(delta_rho_0 < 1e-3);
    CHECK(abs(uqd_burkhard - u_deriv_num) < 1e-3);

}