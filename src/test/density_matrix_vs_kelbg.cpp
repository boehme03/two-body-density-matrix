#include <iostream>
#include <string>
#include <fstream>

#include <matplot/matplot.h>
#include "../density_mat_qs.hpp"
#include "../density_mat_qs_beta_deriv.hpp"
#include "../rapidcsv.h"
#include "../stuff.hpp"

using namespace std;

double Kelbg(double r, double lambda)
{
    double x = r/lambda;
    return -( 1.0 - exp(-x*x) + sqrt(M_PI)*x*( 1.0 - erf(x) ) ) / r;
}


double Kelbg_derivative(double r, double lambda)
{
    double x = r / lambda;
    double ans = (erf(x) - 1.0) / lambda;
    return ans;
}


template <class T>
void write_data_to_csv(const string& filename, const string& header, vector<T> dist_param, vector<T> pot_HTDM, vector<T> pot_Kelbg, vector<T> deriv_HTDM, vector<T> deriv_Kelbg)
{
    ofstream csv;
    csv.open(filename);

    csv << header << "\n";

    for(size_t i = 0; i < dist_param.size(); i++)
    {
        csv << dist_param[i] << ", " << pot_HTDM[i] << ", " << pot_Kelbg[i] << ", " << deriv_HTDM[i] << ", " << deriv_Kelbg[i] <<"\n";
    }

    csv.close();

}



void density_mat_vs_kelbg(double beta, double kappa, double z, int P,double lambda, std::string out_name)
{
    using namespace matplot;
    double q = 0.0; // this is also r in the Kelbg case
    double s = 0.0;
    double dr = 0.1;
    double eps = beta / double(P);
    size_t n_r = 50;

    std::vector<double> res_htdm(n_r);
    std::vector<double> res_kelbg(n_r);

    std::vector<double> res_htdm_deriv(n_r);
    std::vector<double> res_kelbg_deriv(n_r);


    std::vector<double> qs(n_r);
    for(size_t i = 1; i <= n_r; i++)
    {
        q = double(i) * dr;
        qs[i-1] = q;

        res_htdm[i-1] = calc_u_tot(q, s, eps, kappa, z, 5000, 1200, 1e-12) / eps;
        res_kelbg[i-1] = Kelbg(q, lambda);
        res_htdm_deriv[i-1] = u_beta_deriv(q, s, beta, P, kappa, z);
        res_kelbg_deriv[i-1] = Kelbg_derivative(q, lambda);

    }


    string header = "# beta = " + to_string(beta) + ", kappa = " + to_string(kappa) + ", z = " + to_string(z) + ", P =" +
            to_string(P) + ", lambda = " + to_string(lambda);

    write_data_to_csv(out_name, header, qs, res_htdm, res_kelbg, res_htdm_deriv, res_kelbg_deriv);
//    auto fig = figure();
//    tiledlayout(1, 1);

//    fig->size(900, 800);
    //auto ax1 = nexttile();

    //title(ax1, "Potential action HTDM vs Kelbg");
    //ax1->plot(qs, res_htdm, "-o")->display_name("HTDM");
    //hold(on);
    //ax1->plot(qs, res_kelbg, "-x")->display_name("Kelbg");
    //hold(off);
    //ax1->x_axis().label("q (a_B)");
    //ax1->y_axis().label("\\phi(q,s)");

    //legend();

/*
    auto ax2 = nexttile();
    title(ax2, "Potential action derivatives");
    ax2->plot(qs, res_htdm_deriv, "-o")->display_name("HTDM");
    hold(on);
    ax2->plot(qs, res_kelbg_deriv, "-x")->display_name("Kelbg");
    hold(off);
    ax2->x_axis().label("q (q_B)");
    ax2->y_axis().label("u_{derivative}");

    legend();
    show();
    */

}

void HTDM_vs_Kelbg_off_diag()
{
    double rs = 4.0;
    double theta = 1.0;
    double N = 4;

    double m1 = 1.0;
    double m2 = 1836.0;

    double z1 = -1.0;
    double z2 = 1.0;

    double mu =  pow(1./m1 + 1./m2,-1.0);
    double kappa = 1/(2 * mu);
    double z = z1 * z2 / kappa;

    double l = std::pow(4. * M_PI * N / 3.0, 1.0 / 3.0) * rs;

    double beta = 2.0 * l * l * pow(6.0 * N / 2.0 * M_PI * M_PI, -2.0 / 3.0) / theta;

    int P = 1;
    double epsilon = beta / double(P);
    double mass_spec_1 = 1.0;
    double mass_spec_2 = 1836.0;
    double Kelbg_lambda = sqrt(0.5 * epsilon * (1.0 / mass_spec_1 + 1.0 / mass_spec_2));


    // For this test we will keep one Ion at a fixed position
    vector<double> pos_ion = {0.0, l/2.0, 0.0};
    vector<double> pos_elec = {0.0, 0.0, 0.0};

    double dl = l / 200;
    int nl = 200;


    vector<double> s_arr(nl);
    vector<double> res_HTDM(nl);
    vector<double> res_Kelbg(nl);
    vector<double> res_HTMD_deriv(nl);
    vector<double> res_Kelbg_deriv(nl);

    double r1, r2, q,s;

    // Set r1 to the ion position since it stays fixed
    r1 = get_vec_abs(pos_ion);

    for(size_t i = 0; i < (size_t) nl; i++)
    {
        s = get_diff_vec_abs(pos_ion, pos_elec);
        s_arr[i] = s;
        r2 = get_vec_abs(pos_elec);
        q = 0.5 * (r1 + r2);


        res_HTDM[i] = calc_u_tot(q, s, beta, kappa, z, 5000, 1200, 1e-12);
        res_Kelbg[i] = Kelbg(s, Kelbg_lambda);
        res_HTMD_deriv[i] = u_beta_deriv(q, s, beta, P, kappa, z);
        res_Kelbg_deriv[i] = Kelbg_derivative(s, Kelbg_lambda);


        pos_elec[1] += dl;

    }


    string fname = "../../plotting/Kelbg/off-diagonal-e-p.csv";
    string header = "# beta = " + to_string(beta) + ", kappa = " + to_string(kappa) + ", z = " + to_string(z) + ", P =" + to_string(P) + ", lambda = " + to_string(Kelbg_lambda);


    write_data_to_csv(fname, header, s_arr, res_HTDM, res_Kelbg, res_HTMD_deriv, res_Kelbg_deriv);



}


int main()
{
    double rs = 4.0;
    double theta = 1.0;
    double N = 4;

    double m1 = 1.0;
    double m2 = 1836.0;

    double z1 = -1.0;
    double z2 = 1.0;

    double mu =  pow(1./m1 + 1./m2,-1.0);
    double kappa = 1/(2 * mu);
    double z = z1 * z2 / kappa;

    double l = std::pow(4. * M_PI * N / 3.0, 1.0 / 3.0) * rs;
    // This is for the Spin-Unpolarized case

    vector<int> P_vec = {5, 50, 100, 200, 300, 400, 500, 800, 1000, 2000};
    double mass_spec_1 = 1.0;
    double mass_spec_2 = 1836.0;
    for(auto const& P : P_vec) {
        double beta = 2.0 * l * l * pow(6.0 * N / 2.0 * M_PI * M_PI, -2.0 / 3.0) / theta;

        double epsilon = beta / double(P);


        double Kelbg_lambda = sqrt(0.5 * epsilon * (1.0 / mass_spec_1 + 1.0 / mass_spec_2));
        cout << "Beta = " << beta << ", l = " << l << ", z = " << z << ", mu = " << mu << ", eps = " << epsilon <<endl;


        //density_mat_vs_kelbg(beta, kappa, z, P, Kelbg_lambda, "HTDM-vs-Kelbg-P="+ to_string(P)+".csv");
    }


    HTDM_vs_Kelbg_off_diag();
    return 0;
}