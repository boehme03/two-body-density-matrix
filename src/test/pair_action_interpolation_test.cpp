#define CATCH_CONFIG_MAIN
#include <vector>
#include <catch2/catch.hpp>
#include <iostream>
#include <fstream>
#include "../density_mat_qs.hpp"
#include "../rapidcsv.h"
#include "../pair_action_lookup_table.hpp"

TEST_CASE("Test beta derivative interpolation", "pair_action_lookup_table")
{
    double Z1 = 1.0;
    double Z2 = 1.0;

    double m1 = 1.0;
    double m2 = 1.0;


    double beta = 1.25;
    int P = 10;

    double l = 1.0;

    int n_s = 100;
    int n_q = 100;

    int polydeg = 1;

    bool calc_deriv = true;


    pair_action_lookup_table table(Z1, Z1, m1, m2, l, beta, n_s, n_q, polydeg, P, calc_deriv, 5000, 1500, 1e-12, 3.0);
    pair_action_lookup_table table2(Z1, Z1, m1, m2, l, beta, n_s, n_q, polydeg, P, false);

    std::string e_e_file_1("deriv_e-e_comparison.csv");
    std::string e_e_file_2("e-e_comparison.csv");

    double q = 0.5;
    n_s = 30;
    table.print_off_diag_expansion_deriv(q, n_s, e_e_file_1);

    table2.print_off_diag_expansion(q, n_s, e_e_file_2);

}