#pragma once

#include <boost/program_options.hpp>


class Params
{

public:
    Params(double r10_, double r11_, double r20_, double r21_, int n_r1_, int n_r2_, double theta_0_, double theta_1_,
           int n_theta_, double beta_, double Z_1_, double Z_2_, double m1_, double m2_, int n_max_sum_, int n_max_int_);

    double r10;
    double r11;

    double r20;
    double r21;

    int n_r1;
    int n_r2;

    double theta_0;
    double theta_1;
    int n_theta;

    double beta;
    double Z_1, Z_2;
    double Z;
    double kappa, m1, m2;
    int n_max_sum, n_max_int;
    double tol_sum;
    void print_configuration();

};


Params::Params(double r10_, double r11_, double r20_, double r21_, int n_r1_, int n_r2_,
                   double theta_0_, double theta_1_, int n_theta_, double beta_, double Z_1_, double Z_2_, double m1_,
                   double m2_, int n_max_sum_, int n_max_int_)
        : r10(r10_), r11(r11_), r20(r20_), r21(r21_), n_r1(n_r1_), n_r2(n_r2_), theta_0(theta_0_),
          theta_1(theta_1_), n_theta(n_theta_), beta(beta_), Z_1(Z_1_), Z_2(Z_2_),
          m1(m1_), m2(m2_), n_max_sum(n_max_sum_), n_max_int(n_max_int_)
{

    double mu = m1*m2/ (m1 + m2);

    kappa = 1 / ( 2 * mu);

    Z = Z_1 * Z_2 / kappa;
    tol_sum = 1e-12;
};


void Params::print_configuration()
{
    using namespace std;

    cout << "r10 = " << r10 << " ,r11 = " << r11 <<", r20 = " << r20 << ", n_r1 = " << n_r1 << ", n_r2 = " << n_r2 << std::endl;
    cout << "theta_0 = " << theta_0 << ", theta_1 = " << theta_1 << ", n_theta = " << n_theta << std::endl;
    cout << "m_1 = " << m1 << ", m_2 = " << m2 << ", Z_1 = " << Z_1  << ", Z_2 = " << Z_2 << ", beta = " << beta << std::endl;
    cout << "kappa = " << kappa << ", Z = " << Z << std::endl;
}


