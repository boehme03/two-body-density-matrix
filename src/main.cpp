
#ifdef NUM_DEBUG

#define ENABLE_NUM_ERR_LOG true


#endif

#ifndef NUM_DEBUG

#define ENABLE_NUM_ERR_LOG false

#endif


#include <iostream>
#include <string>
#include <limits>
#include <iomanip>
#include <ostream>
#include <fstream>

#include "density_mat.hpp"
#include "cfg_file_handler.hpp"
#include "density_mat_qs.hpp"
#include "pair_action_lookup_table.hpp"

using namespace std;


int main(int arg_c, char* argv[])
{
    double Z1 = 1.0;
    double Z2 = 1.0;

    double m1 = 1.0;
    double m2 = 1.0;


    double beta = 0.125;
    int P = 1;

    double l = 2;

    int n_s = 100;
    int n_q = 100;

    int polydeg = 1;

    bool calc_deriv = true;

    std::string fname1 = "pair_action_coefficients.csv";
    std::string fname2 = "pair_action_deriv_coefficients.csv";



    double epsilon = beta / P;
    // Initialize the data-structures
    double mu = m1 * m2 / ( m1 + m2);
    double kappa = 1/(2 * mu);
    double z = Z1*Z2 / kappa;
    double lambda = sqrt(4*kappa*epsilon);
    // set up the necessary values for the q grid
    double q = 0.0;
    double q_max = sqrt(3.0) * l / 2;
    double q_min = 0.0;
    double dq = (q_max - q_min) / (n_q - 1);

    std::vector<double> bead1 = {dq*10, 0.0, 0.0};
    std::vector<double> bead2 = {0.0, dq*5, 0.0};



    double r_1 = sqrt(bead1[0]*bead1[0]+ bead1[1]*bead1[1] + bead1[2]*bead1[2]);
    double r_2 = sqrt(bead2[0]*bead2[0]+ bead2[1]*bead2[1] + bead2[2]*bead2[2]);

    double q1 = 0.5 * (r_1 + r_2);
    double s1 = sqrt( std::pow(bead1[0]-bead2[0],2) + std::pow(bead1[1]-bead2[1],2) + std::pow(bead1[2]-bead2[2],2));

    pair_action_lookup_table table(fname1, fname2, Z1, Z2, m1, m2, l, beta, n_s, n_q, polydeg, P);

    auto res = table.get_pair_action(bead1, bead2);

    double delta_beta = 1e-6;
    double u_qs_ana = calc_u_tot(q1, s1, epsilon, kappa, z, 5000, 1500, 1e-12);
    double u_qs2 = calc_u_tot(q1, s1, epsilon + delta_beta, kappa, z, 5000, 1500, 1e-12);

    double u_deriv_num = 1./P * (( u_qs2 - u_qs_ana) / delta_beta);


    double u_deriv_ana = u_beta_deriv(q1, s1, beta, P, kappa, z);

    std::cout << res[0] << ", " << res[1] << std::endl;
    std::cout << u_qs_ana << ", " << u_deriv_ana << std::endl;
    std::cout << "Numerical derivative: u_deriv_num = " << u_deriv_num << std::endl;

    return 0;
}
