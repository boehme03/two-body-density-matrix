#include <vector>
#include <iostream>
#include <cmath>

#include "rapidcsv.h"
#include "density_mat_qs.hpp"

double calc_absolute_val(std::vector<double>& v)
{
    double res = 0.0;
    for(int i = 0; i < 3; i++)
    {
        res += v[i] * v[i];
    }

    return sqrt(res);
}

double calc_diff_val(std::vector<double>& v1, std::vector<double>& v2)
{
    double res = 0.0;

    for(int i = 0; i < 3; i++)
    {
        res += std::pow(v1[i] - v2[i], 2);
    }

    return sqrt(res);
}


int main()
{
    rapidcsv::Document off_diag("offdiag_HTDM_Snapshot_analysis.dat", rapidcsv::LabelParams(-1,-1), rapidcsv::SeparatorParams('\t'));

    std::vector<double> x1 = off_diag.GetColumn<double>(4);
    std::vector<double> x2 = off_diag.GetColumn<double>(5);


    double m1 = 1.0;
    double m2 = 1836.15;
    double beta = 2.17204;
    double P = 100;
    double epsilon = beta / P;
    double z1 = -1.0;
    double z2 = 1.0;

    double mu = m1 * m2 / (m1 + m2);

    double kappa = 1 / (2 * mu);

    double z = z1 * z2 / kappa;

    std::ofstream out_file;
    out_file.open("../../plotting/pair_action_benchmark_data.csv");

    for(int i = 0; i < x1.size(); i++)
    {
        std::vector<double> vec1 = {x1[i], 0, 0};
        std::vector<double> vec2 = {x2[i], 0, 0};


        double r1 = calc_absolute_val(vec1);
        double r2 = calc_absolute_val(vec2);
        double q = 0.5 * (r1 + r2);
        double s = calc_diff_val(vec1, vec2);
        std::cout << "q = " << q <<", s = "<< s <<", r1 = " << r1 << ", r2 = " << r2 << std::endl;

        double rho1 = rho_coulomb_qs(q, s, epsilon, kappa, z, 5000, 1200, 1e-12);
        double rhof = rho_free_qs(s, kappa, epsilon);
        double ufree = u_free_qs(s, kappa, epsilon);
        double u = ufree - log(rho1);

        out_file << q << ", " << s << ", " << u / epsilon << ", " << rho1 << ", " << rhof << ", " << ufree << std::endl;

    }
    out_file.close();

    return 0;
}