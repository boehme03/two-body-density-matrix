#=
Simple plot script for the benchmark data in Pollock1998
=#

using Plots
using Pkg
using CSV
using DataFrames
using LaTeXStrings
using Gnuplot


# Loading the data into memory

df = CSV.read("plotting/rhom_vs_adap_e-e_pollock_diag.csv",DataFrame, header=false);
df2 = CSV.read("plotting/data_pub/pollock_action_e_e.csv", DataFrame);

data_mat = Matrix(df);
data_pub = Matrix(df2);

# define sime helper functions to switch between the action and the pair-density matrix

function u_free_fun(Z, r1, r2, costheta, beta, kappa)
    
    exponent = - (r1^2 + r2^2 - 2 * r1 * r2 * costheta) / (4 * kappa * beta)
    pref = -3/2. * log(4 *pi * kappa* beta)
    
    return pref + exponent

end;

function rho_free_fun(Z, r1, r2, costheta, beta, kappa)
    exponent = - (r1^2 + r2^2 - 2 * r1 * r2 * costheta) / (4.0 * kappa * beta)
    pref = (4.0 * pi * kappa * beta )^(-3/2)
    return pref * exp(exponent)
end;

function calc_rho_from_action(u, rho_free)
    return exp(-u) * rho_free
end;

# Convert the action into a density matrix

r = data_pub[:,1];
rho_free_pollock = rho_free_fun.(1.0, r, r, 1.0, 1.0, 1.0);
rho_pollock = calc_rho_from_action.(data_pub[:,2], rho_free_pollock);

# Convert the calculated densiy matrix into an action

u_free_data = u_free_fun.(1.0, r, r, 1.0, 1.0, 1.0)
u_diag_adap = u_free_data - log.(data_mat[:,2])  


# Calculate the relative errors compared to the data points 

err1 = abs.(data_mat[:,2] - rho_pollock) ./ rho_pollock
err2 = abs.(u_diag_adap - data_pub[:,2]) ./ data_pub[:,2] 

err3 = abs.(data_mat[:,3] - rho_pollock) ./ rho_pollock
err4 = abs.(data_mat[:,5] - data_pub[:,2]) ./ data_pub[:,2]

plot_font = "Computer Modern";
default(
  fontfamily=plot_font,
  linewidth=2, 
  framestyle=:box, 
  label=nothing, 
  grid=false,
  show=true,
  palette=:default
)


p1 = plot(data_mat[:,1], data_mat[:,2], markershape=:circle,markersize=6, markerstrokecolor=:blue, markerstrokewidth=2, markercolor=:white, ylabel=L"$\rho(\mathbf{r},\mathbf{r};\beta)$", label="adaptive", annotations=(0.1, 0.01, Plots.text(L"(a)", 8, :left)));
plot!(r, data_mat[:,2], markershape=:cross, markersize=6, markerstrokewidth=2, linecolor=:red, linewidth=1.0, markerstrokecolor=:red, markercolor=:red, label="Romberg")
plot!(r, rho_pollock, markersize=2, markershape=:square,  label="Pollock 1988",foreground_color_legend = nothing, legend_position=:bottomright);

p2 = plot(data_mat[:,1], u_diag_adap, markershape=:circle, markersize=6, markerstrokecolor=:blue, markerstrokewidth=2, markercolor=:white, ylabel=L"u($\mathbf{r},\mathbf{r};\beta$)", label="adaptive", annotations=(2.0, 1.1, Plots.text(L"(b)", 8, :left)))
plot!(r, data_mat[:,5], markershape=:cross, markersize=6, markerstrokewidth=2, linecolor=:red, linewidth=1.0, markerstrokecolor=:red, markercolor=:red, label="Romberg")
plot!(data_mat[:,1], data_pub[:,2], markershape=:square, markersize=2, markerstrokecolor=:match, xlabel=L"r [a.u.]", label="Pollock 1988", foreground_color_legend = nothing)


p3 = plot(data_mat[:,1], err1, markershape=:circle, markersize=2, markerstrokecolor=:blue, markerstrokewidth=2, markercolor=:match,  ylabel=L"$\Delta\rho(\mathbf{r},\mathbf{r};\beta)$ rel.", label="adaptive", foreground_color_legend = nothing, annotations=(0.1, 0.0008, Plots.text(L"(c)", 8, :left)));
plot!(r, err3, markershape=:cross, markersize=6, markerstrokewidth=2, linecolor=:red, linewidth=1.0, markerstrokecolor=:red, markercolor=:red, label="Romberg", legendfontsize=8, legend=:topleft)

p4 = plot(data_mat[:,1], err2, foreground_color_legend = nothing,  markershape=:circle, markersize=2, markerstrokecolor=:blue, markerstrokewidth=2, markercolor=:match, xlabel=L"r [a.u.]",  ylabel=L"$\Delta$u($\mathbf{r},\mathbf{r};\beta$) rel.", label="adaptive", annotations=(0.1, 0.0015, Plots.text(L"(d)", 8, :left)))
plot!(r, err4, markershape=:cross, markersize=6, markerstrokewidth=2, linecolor=:red, linewidth=1.0, markerstrokecolor=:red, markercolor=:red, label="Romberg", legendfontsize=8)


psave = plot(p1, p3, p2, p4, layout=(2,2))

savefig(psave, "plotting/diss/Comparison_Pollock_e-e_diag.pdf")
savefig(psave, "~/work/Dissertation/fig/ch3/Comparison_Pollock_e-e_diag.pdf")