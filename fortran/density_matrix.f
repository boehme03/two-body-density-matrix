!************************************************************************************************
!*wrapper for Pollock's function to compute the density matrix					*
!*	by J. Vorberger										*
!************************************************************************************************
!needs coulcc_lib.f
! gfortran -fdefault-real-8 density_matrix.f coulcc_lib.f -o density_matrix.out
!

	program density_matrix_test
	
	real r1,r2,theta,beta,xkappa,z
!	rhocoul
		
	write(*,*) 'r1[a_B]='
	read(*,*) r1
	write(*,*) 'r2[a_B]='
	read(*,*) r2
	write(*,*) 'theta='
	read(*,*) theta
	write(*,*) 'beta'
	read(*,*) beta
	write(*,*) 'xkappa='
	read(*,*) xkappa
	write(*,*) 'z='
	read(*,*) z
	
	tmp=rhocoul(r1,r2,theta,beta,xkappa,z)
	write(*,*) tmp
	
	end




!************************************************************************************************
!* Pollock 1988m function rhocoul								*
!*	This function returns the two-particle relative density matrix for the Coulomb potential*
!*												*
!*	r1	"initial" radius [a_B]								*
!*	r2	"final" radius [a_B]								*
!*	theta	angle between r1 & r2								*
!*	beta	inverse temperature [e^2/a_B]							*
!*	xkappa	hbar^2/(2*reduced mass * e^2 * a_B)						*
!*	z	Z1*Z2/xkappa (can be positive or negative)					*
!************************************************************************************************
!needs coulcc_lib.f

	function rhocoul(r1,r2,theta,beta,xkappa,z)
	
	common /specs/ r11,r22,ttheta,bbeta,xkap,zz
	external cgrand
	
	r11=r1
	r22=r2
	ttheta=theta
	bbeta=beta
	xkap=xkappa
	zz=z
	uklim=sqrt(5./(beta*xkappa))
!	write(*,*) uklim
	call qromb(cgrand,0.0,2.*uklim,contin)
	rhocoul=contin
!	write(*,*) rhocoul
	if(z.ge.0.0) return
	bstuf=bound(r1,r2,theta,beta,xkappa,z,n)
	rhcoul=contin+bstuf
!	write(*,*) rhocoul
	return
	end


!************************************************************************************************
!* Pollock 1988, function bound									*
!*	returns bound state contribution to 3D Coulomb matrix					*
!*	sum over principal quantum numbers is terminated when the contribution is less than	*
!*	epsbd of the total accumulated bound state sum or when n exceeds nmax (set in data 	*
!*	statement)										*
!************************************************************************************************

	function bound(r1,r2,theta,beta,xkappa,z,n)

	data fourpii/.079577472/epsbd/1.e-19/nmax/300/

	sum =0.0
	r12=sqrt(r1*r1+r2*r2-2.*r1*r2*cos(theta))
	x=(r1+r2+r12)/2.
	y=(r1+r2-r12)/2.
	zabs=abs(z)
!	write(*,*) r12,x,y,zabs
	
	do n=1,nmax
		fxp=1.0
		zxn=zabs*x/float(n)
		fx=1.-zxn
		
		fyp=1.0
		zyn=zabs*y/float(n)
		fy=1.-zyn
		
		if (n.ne.1) then
			do i=1,n-1
				fxn=((2.0*i+1-zxn)*fx-i*fxp)/(i+1)
				fxp=fx
				fx=fxn
				if (x.ne.y) then
				 fyn=((2.0*i+1-zyn)*fy-i*fyp)/(i+1)
				 fyp=fy
				 fy=fyn
				endif
			end do
		endif
		prefac=fourpii*exp(0.25*beta*xkappa*z*z/(n*n))*
     &		(0.5*zabs**3./n**5.)*exp(-0.5*zabs*(r1+r2)/n)
		if(x.ne.y) then
			term=(n**3/zabs)*prefac*(fxp*fy-fx*fyp)/r12
		else
			if (x.ne.0.0) then
				term=(n**2)*prefac*
     &				((n**2/(zabs*r1))*(fx-fxp)**2+fx*fxp)
			else
				term=(n**2)*prefac
			endif
		endif
		sum=sum+term
		oldbound=bound
		tail=(n-1)*(n+1)*term/float(2*n-1)
		bound=sum+tail
!		write(*,*) n,bound,oldbound
	if (abs(bound-oldbound).lt.epsbd*abs(bound)) EXIT
	end do
	
	return
	end


!************************************************************************************************
!* Pollock 1988, cgrand integration of scattering function					*
!*	integrand for continuum part of the 3D Coulomb density matrix				*
!************************************************************************************************

	function cgrand(wavek)
	
	common /specs/ r1,r2,theta,beta,xkappa,z
	complex fc(1),fcp(1),gc(1),gcp(1),sig(1),xkz,ykz,eta1
	data pi/3.141592654/twopi/6.283185308/twopisq/19.73920881/
	
	cgrand=0.0
	if(wavek.eq.0.0) return
	
	r12=sqrt(r1*r1+r2*r2-2.*r1*r2*cos(theta))
	x=(r1+r2+r12)/2.
	y=(r1+r2-r12)/2.
	xk=wavek*x
	yk=wavek*y
	xkz=cmplx(xk)
	ykz=cmplx(yk)
	gibbs=exp(-beta*xkappa*wavek**2)
	fo1=z/(2.0*wavek)
	eta1=cmplx(fo1,0.0)
!	write(*,*) 'cgrand:',x,y
!	write(*,*) 'cgrand:',xkz,ykz,eta1,sig,ifail
	
	if (x.ne.y) then
		call coulcc(xkz,eta1,0,1,fc,gc,fcp,gcp,sig,3,0,ifail)
		fx=real(fc(1))
		fxd=real(fcp(1))
!		write(*,*) 'cgrand=',fx,fxd
		if (y.lt.1.e-8) then
			fy=0.0
			fyd=sqrt(twopi*fo1/(exp(twopi*fo1)-1.))
		else
		call coulcc(ykz,eta1,0,1,fc,gc,fcp,gcp,sig,3,0,ifail)
		 fy=real(fc(1))
		 fyd=real(fcp(1))
		endif
		cgrand=wavek*gibbs*(fx*fyd-fy*fxd)/(twopisq*r12)
!		write(*,*) cgrand
		return
	else
		if (x.ne.0.) then
		call coulcc(xkz,eta1,0,1,fc,gc,fcp,gcp,sig,3,0,ifail)
		 fx=real(fc(1))
		 fxd=real(fcp(1))
		 fo2=wavek**2*gibbs*(fxd**2+
     &                  (1.-z/(wavek*wavek*r1))*fx*fx)/twopisq
		 cgrand=fo2
		else
		 cgrand=(z/twopi)*wavek*gibbs/(exp(z*pi/wavek)-1.)
		endif
!		write(*,*) cgrand
		return
	endif
	
	return
	end

!-----------------------------------------------------------------------------------------------
	SUBROUTINE qromb(func,a,b,ss)
	INTEGER JMAX,JMAXP,K,KM
	REAL a,b,func,ss,EPS
	EXTERNAL func
	PARAMETER (EPS=1.e-3, JMAX=20, JMAXP=JMAX+1, K=5, KM=K-1)
!USES  polint,trapzd 
!Returns as ss the integral of the function func from a to b. 
!Integration is performed by Romberg�s method of order 2K,
!where,e.g.,K=2 is Simpson�s rule.
!Parameters: 
!EPS is the fractional accuracy desired, as determined by the extrapolation error estimate;
!JMAX limits the total number of steps;
!K is the number of points used inthe extrapolation.

	INTEGER j
	REAL dss,h(JMAXP),s(JMAXP)
	h(1)=1.
	do j=1,JMAX
		call trapzd(func,a,b,s(j),j)
		if (j.ge.K) then
			call polint(h(j-KM),s(j-KM),K,0.,ss,dss)
			if (abs(dss).le.EPS*abs(ss)) return
		endif
		s(j+1)=s(j)
		h(j+1)=0.25*h(j)
	end do
	pause 'too many steps in qromb'
	return
	END
	
!-----------------------------------------------------------------------------------------------
	SUBROUTINE trapzd(func,a,b,s,n)
	INTEGER n
	REAL a,b,s,func
	EXTERNAL func
!This routine computes the nth stage of refinement of an extended trapezoidal rule.
!func isinput as the name of the function to be integrated between limits a and b, also input. 
!When called withn=1, the routine returns as s the crudest estimate of Rbaf(x)dx.
!Subsequent calls with n=2,3,... (in that sequential order) will improve the accuracy of s by 
!adding 2n-2 additional interior points. s should not be modified between sequential calls.
	INTEGER it,j
	REAL del,sum,tnm,x
	
	if (n.eq.1) then
		s=0.5*(b-a)*(func(a)+func(b))
	else
		it=2**(n-2)
		tnm=it
		del=(b-a)/tnm
		x=a+0.5*del
		sum=0.
		do j=1,it
			sum=sum+func(x)
			x=x+del
		enddo
		s=0.5*(s+(b-a)*sum/tnm)
	endif
	return
	END
	
!-----------------------------------------------------------------------------------------------
      SUBROUTINE polint(xa,ya,n,x,y,dy)
      INTEGER n,NMAX
      REAL dy,x,y,xa(n),ya(n)
      PARAMETER (NMAX=10)
      INTEGER i,m,ns
      REAL den,dif,dift,ho,hp,w,c(NMAX),d(NMAX)

      ns=1
      dif=abs(x-xa(1))
      do 11 i=1,n
        dift=abs(x-xa(i))
        if (dift.lt.dif) then
          ns=i
          dif=dift
        endif
        c(i)=ya(i)
        d(i)=ya(i)
11    continue
      y=ya(ns)
      ns=ns-1
      do 13 m=1,n-1
        do 12 i=1,n-m
          ho=xa(i)-x
          hp=xa(i+m)-x
          w=c(i+1)-d(i)
          den=ho-hp
          if(den.eq.0.)stop 'failure in polint'
          den=w/den
          d(i)=hp*den
          c(i)=ho*den
12      continue
        if (2*ns.lt.n-m)then
          dy=c(ns+1)
        else
          dy=d(ns)
          ns=ns-1
        endif
        y=y+dy
13    continue
      return
      END
!  (C) Copr. 1986-92 Numerical Recipes Software 5.)'j)&3|,$.